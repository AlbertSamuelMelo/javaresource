package com.example.myapplication.tables;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.evolucao.rmlibrary.database.ComboBox;
import com.evolucao.rmlibrary.database.Database;
import com.evolucao.rmlibrary.database.ExistenceCheck;
import com.evolucao.rmlibrary.database.Field;
import com.evolucao.rmlibrary.database.ForeingSearch;
import com.evolucao.rmlibrary.database.InternalSearch;
import com.evolucao.rmlibrary.database.RmGridRegistry;
import com.evolucao.rmlibrary.database.Table;
import com.evolucao.rmlibrary.database.TableRegistry;
import com.evolucao.rmlibrary.database.enumerators.FieldType;
import com.evolucao.rmlibrary.database.events.AfterExistenceCheckEvent;
import com.evolucao.rmlibrary.database.events.AfterExistenceCheckEvent.AfterExistenceCheckEventListener;
import com.evolucao.rmlibrary.database.events.AfterValidateOnEditEvent;
import com.evolucao.rmlibrary.database.events.AfterValidateOnEditEvent.AfterValidateOnEditEventListener;
import com.evolucao.rmlibrary.database.events.BeforeForeingSearchEvent;
import com.evolucao.rmlibrary.database.events.BeforeForeingSearchEvent.BeforeForeingSearchEventListener;
import com.evolucao.rmlibrary.database.events.ComboBoxItemCaptionGeneratorEvent;
import com.evolucao.rmlibrary.database.events.ComboBoxItemCaptionGeneratorEvent.ComboBoxItemCaptionGeneratorEventListener;
import com.evolucao.rmlibrary.database.events.CommandEvent;
import com.evolucao.rmlibrary.database.events.CommandEvent.CommandEventListener;
import com.evolucao.rmlibrary.database.events.LoadRmGridEvent;
import com.evolucao.rmlibrary.database.events.LoadRmGridEvent.LoadRmGridEventListener;
import com.evolucao.rmlibrary.database.events.LoadTableEvent;
import com.evolucao.rmlibrary.database.events.LoadTableEvent.LoadTableEventListener;
import com.evolucao.rmlibrary.database.events.table.AfterFilterEvent;
import com.evolucao.rmlibrary.database.events.table.AfterFilterEvent.AfterFilterEventListener;
import com.evolucao.rmlibrary.database.events.table.AfterInsertEvent;
import com.evolucao.rmlibrary.database.events.table.AfterUpdateEvent;
import com.evolucao.rmlibrary.database.events.table.AfterUpdateEvent.AfterUpdateEventListener;
import com.evolucao.rmlibrary.database.events.table.AfterInsertEvent.AfterInsertEventListener;
import com.evolucao.rmlibrary.database.events.table.InitialValueEvent;
import com.evolucao.rmlibrary.database.events.table.InitialValueEvent.InitialValueEventListener;
import com.evolucao.rmlibrary.database.events.table.SpecialConditionOfDeleteEvent;
import com.evolucao.rmlibrary.database.events.table.SpecialConditionOfDeleteEvent.SpecialConditionOfDeleteEventListener;
import com.evolucao.rmlibrary.database.registry.CommandRegistry;
import com.evolucao.rmlibrary.ui.controller.ControlButton;
import com.evolucao.rmlibrary.ui.controller.ControlForm;
import com.evolucao.rmlibrary.ui.controller.events.ControlButtonClickEvent;
import com.evolucao.rmlibrary.ui.controller.events.ControlButtonClickEvent.ControlButtonClickEventListener;
import com.evolucao.rmlibrary.ui.form.enumerators.SectionState;
import com.evolucao.rmlibrary.ui.production.RmGrid;
import com.evolucao.rmlibrary.utils.Utils;
import com.evolucao.rmlibrary.window.RmFormButtonBase;
import com.evolucao.rmlibrary.window.RmFormWindow;
import com.evolucao.rmlibrary.window.event.RmFormButtonClickEvent;
import com.evolucao.rmlibrary.window.event.RmFormButtonClickEvent.RmFormButtonClickEventListener;
import com.evolucao.weblibrary.ApplicationUI;
import com.example.myapplication.view.Agenda2View;
import com.vaadin.ui.UI;

public class MarcCons {
	public static void configure(Database database) {
		TableRegistry tableRegistry = database.addTableRegistry("marccons");
		tableRegistry.addLoadTableEventListener(new LoadTableEventListener() {
			@Override
			public void onLoadTable(LoadTableEvent event) {
				Table tblTabela = event.getTable();
				tblTabela.setDebugQuery(true);
				tblTabela.setTableName("marccons");
				tblTabela.addField("sequencia", FieldType.INTEGER, 10);
				tblTabela.addField("codiasso", FieldType.INTEGER, 10);
				tblTabela.addField("uidassociado", FieldType.VARCHAR, 50);
				tblTabela.addField("nome", FieldType.VARCHAR, 50);
				tblTabela.addField("paciente", FieldType.VARCHAR, 50);
				tblTabela.addField("nasc", FieldType.DATE, 10);
				tblTabela.addField("cortesia", FieldType.VARCHAR, 1);
				tblTabela.addField("uidusualibe", FieldType.VARCHAR, 50);
				
				tblTabela.addField("codiespe", FieldType.INTEGER, 10);
				tblTabela.addField("desccodiespe", FieldType.VARCHAR, 50);
				tblTabela.addField("codiespe_valor", FieldType.DOUBLE, 10);
				tblTabela.addField("codiespe_vlrcusto", FieldType.DOUBLE, 10);
				
				tblTabela.addField("ulticons", FieldType.DATE, 10);
				tblTabela.addField("ulticonsdias", FieldType.INTEGER, 10);
				tblTabela.addField("ultitipoatend", FieldType.VARCHAR, 10);
				tblTabela.addField("ultidesctipoatend", FieldType.VARCHAR, 50);
				tblTabela.addField("ultiatendsequ", FieldType.INTEGER, 10);
				
				tblTabela.addField("tipoatend", FieldType.VARCHAR, 1);
				tblTabela.addField("desctipoatend", FieldType.VARCHAR, 50);
				tblTabela.addField("atendido", FieldType.VARCHAR, 1);
				tblTabela.addField("datacons", FieldType.DATE, 10);
				tblTabela.addField("diasema", FieldType.VARCHAR, 50);
				tblTabela.addField("horacons", FieldType.VARCHAR, 5);
				tblTabela.addField("codimedi", FieldType.INTEGER, 10);
				tblTabela.addField("desccodimedi", FieldType.VARCHAR, 50);
				tblTabela.addField("numeproc", FieldType.INTEGER, 10);
				
				tblTabela.addField("uidfornecedor", FieldType.VARCHAR, 50);
				
				tblTabela.addField("altura", FieldType.VARCHAR, 10);
				tblTabela.addField("peso", FieldType.VARCHAR, 10);
				tblTabela.addField("pressao", FieldType.VARCHAR, 10);
				tblTabela.addField("pulsacao", FieldType.VARCHAR, 10);
				tblTabela.addField("temperatura", FieldType.VARCHAR, 10);
				
				tblTabela.addField("codiproc01", FieldType.INTEGER, 10);
				tblTabela.addField("codiproc01_valor", FieldType.DOUBLE, 10);
				tblTabela.addField("codiproc01_vlrcusto", FieldType.DOUBLE, 10);
				
				tblTabela.addField("codiproc02", FieldType.INTEGER, 10);
				tblTabela.addField("codiproc02_valor", FieldType.DOUBLE, 10);
				tblTabela.addField("codiproc02_vlrcusto", FieldType.DOUBLE, 10);
				
				tblTabela.addField("codiproc03", FieldType.INTEGER, 10);
				tblTabela.addField("codiproc03_valor", FieldType.DOUBLE, 10);
				tblTabela.addField("codiproc03_vlrcusto", FieldType.DOUBLE, 10);
				
				tblTabela.addField("codiproc04", FieldType.INTEGER, 10);
				tblTabela.addField("codiproc04_valor", FieldType.DOUBLE, 10);
				tblTabela.addField("codiproc04_vlrcusto", FieldType.DOUBLE, 10);
				
				tblTabela.addField("codiproc05", FieldType.INTEGER, 10);
				tblTabela.addField("codiproc05_valor", FieldType.DOUBLE, 10);
				tblTabela.addField("codiproc05_vlrcusto", FieldType.DOUBLE, 10);
				
				tblTabela.addField("codiproc06", FieldType.INTEGER, 10);
				tblTabela.addField("codiproc06_valor", FieldType.DOUBLE, 10);
				tblTabela.addField("codiproc06_vlrcusto", FieldType.DOUBLE, 10);
				
				tblTabela.addField("codiproc07", FieldType.INTEGER, 10);
				tblTabela.addField("codiproc07_valor", FieldType.DOUBLE, 10);
				tblTabela.addField("codiproc07_vlrcusto", FieldType.DOUBLE, 10);
				
				tblTabela.addField("codiproc08", FieldType.INTEGER, 10);
				tblTabela.addField("codiproc08_valor", FieldType.DOUBLE, 10);
				tblTabela.addField("codiproc08_vlrcusto", FieldType.DOUBLE, 10);
				
				tblTabela.addField("codiproc09", FieldType.INTEGER, 10);
				tblTabela.addField("codiproc09_valor", FieldType.DOUBLE, 10);
				tblTabela.addField("codiproc09_vlrcusto", FieldType.DOUBLE, 10);
				
				tblTabela.addField("codiproc10", FieldType.INTEGER, 10);
				tblTabela.addField("codiproc10_valor", FieldType.DOUBLE, 10);
				tblTabela.addField("codiproc10_vlrcusto", FieldType.DOUBLE, 10);
				
				tblTabela.addField("codiproc11", FieldType.INTEGER, 10);
				tblTabela.addField("codiproc11_valor", FieldType.DOUBLE, 10);
				tblTabela.addField("codiproc11_vlrcusto", FieldType.DOUBLE, 10);
				
				tblTabela.addField("codiproc12", FieldType.INTEGER, 10);
				tblTabela.addField("codiproc12_valor", FieldType.DOUBLE, 10);
				tblTabela.addField("codiproc12_vlrcusto", FieldType.DOUBLE, 10);
				
				tblTabela.addField("codiproc13", FieldType.INTEGER, 10);
				tblTabela.addField("codiproc13_valor", FieldType.DOUBLE, 10);
				tblTabela.addField("codiproc13_vlrcusto", FieldType.DOUBLE, 10);
				
				tblTabela.addField("codiproc14", FieldType.INTEGER, 10);
				tblTabela.addField("codiproc14_valor", FieldType.DOUBLE, 10);
				tblTabela.addField("codiproc14_vlrcusto", FieldType.DOUBLE, 10);
				
				tblTabela.addField("codiproc15", FieldType.INTEGER, 10);
				tblTabela.addField("codiproc15_valor", FieldType.DOUBLE, 10);
				tblTabela.addField("codiproc15_vlrcusto", FieldType.DOUBLE, 10);
				
				tblTabela.addField("codiproc16", FieldType.INTEGER, 10);
				tblTabela.addField("codiproc16_valor", FieldType.DOUBLE, 10);
				tblTabela.addField("codiproc16_vlrcusto", FieldType.DOUBLE, 10);

				tblTabela.addField("codiproc17", FieldType.INTEGER, 10);
				tblTabela.addField("codiproc17_valor", FieldType.DOUBLE, 10);
				tblTabela.addField("codiproc17_vlrcusto", FieldType.DOUBLE, 10);
				
				tblTabela.addField("codiproc18", FieldType.INTEGER, 10);
				tblTabela.addField("codiproc18_valor", FieldType.DOUBLE, 10);
				tblTabela.addField("codiproc18_vlrcusto", FieldType.DOUBLE, 10);
				
				tblTabela.addField("codiproc19", FieldType.INTEGER, 10);
				tblTabela.addField("codiproc19_valor", FieldType.DOUBLE, 10);
				tblTabela.addField("codiproc19_vlrcusto", FieldType.DOUBLE, 10);
				
				tblTabela.addField("codiproc20", FieldType.INTEGER, 10);
				tblTabela.addField("codiproc20_valor", FieldType.DOUBLE, 10);
				tblTabela.addField("codiproc20_vlrcusto", FieldType.DOUBLE, 10);

				tblTabela.addField("codiproc21", FieldType.INTEGER, 10);
				tblTabela.addField("codiproc21_valor", FieldType.DOUBLE, 10);
				tblTabela.addField("codiproc21_vlrcusto", FieldType.DOUBLE, 10);
				
				tblTabela.addField("codiproc22", FieldType.INTEGER, 10);
				tblTabela.addField("codiproc22_valor", FieldType.DOUBLE, 10);
				tblTabela.addField("codiproc22_vlrcusto", FieldType.DOUBLE, 10);
				
				tblTabela.addField("codiproc23", FieldType.INTEGER, 10);
				tblTabela.addField("codiproc23_valor", FieldType.DOUBLE, 10);
				tblTabela.addField("codiproc23_vlrcusto", FieldType.DOUBLE, 10);

				tblTabela.addField("codiproc24", FieldType.INTEGER, 10);
				tblTabela.addField("codiproc24_valor", FieldType.DOUBLE, 10);
				tblTabela.addField("codiproc24_vlrcusto", FieldType.DOUBLE, 10);
				
				tblTabela.addField("codiproc25", FieldType.INTEGER, 10);
				tblTabela.addField("codiproc25_valor", FieldType.DOUBLE, 10);
				tblTabela.addField("codiproc25_vlrcusto", FieldType.DOUBLE, 10);
				
				tblTabela.addField("codiproc26", FieldType.INTEGER, 10);
				tblTabela.addField("codiproc26_valor", FieldType.DOUBLE, 10);
				tblTabela.addField("codiproc26_vlrcusto", FieldType.DOUBLE, 10);
				
				tblTabela.addField("codiproc27", FieldType.INTEGER, 10);
				tblTabela.addField("codiproc27_valor", FieldType.DOUBLE, 10);
				tblTabela.addField("codiproc27_vlrcusto", FieldType.DOUBLE, 10);
				
				tblTabela.addField("codiproc28", FieldType.INTEGER, 10);
				tblTabela.addField("codiproc28_valor", FieldType.DOUBLE, 10);
				tblTabela.addField("codiproc28_vlrcusto", FieldType.DOUBLE, 10);
				
				tblTabela.addField("codiproc29", FieldType.INTEGER, 10);
				tblTabela.addField("codiproc29_valor", FieldType.DOUBLE, 10);
				tblTabela.addField("codiproc29_vlrcusto", FieldType.DOUBLE, 10);
				
				tblTabela.addField("codiproc30", FieldType.INTEGER, 10);
				tblTabela.addField("codiproc30_valor", FieldType.DOUBLE, 10);
				tblTabela.addField("codiproc30_vlrcusto", FieldType.DOUBLE, 10);
				
				tblTabela.addField("codiproc31", FieldType.INTEGER, 10);
				tblTabela.addField("codiproc31_valor", FieldType.DOUBLE, 10);
				tblTabela.addField("codiproc31_vlrcusto", FieldType.DOUBLE, 10);
				
				tblTabela.addField("codiproc32", FieldType.INTEGER, 10);
				tblTabela.addField("codiproc32_valor", FieldType.DOUBLE, 10);
				tblTabela.addField("codiproc32_vlrcusto", FieldType.DOUBLE, 10);
				
				tblTabela.addField("vlrtotal", FieldType.DOUBLE, 10);
				tblTabela.addField("vlrcustototal", FieldType.DOUBLE, 10);
				tblTabela.addField("obsreci", FieldType.TEXT, 250);
				tblTabela.addField("observacoes", FieldType.TEXT, 250);
				
				tblTabela.addField("uidcarrcomp", FieldType.VARCHAR, 50);
				
				tblTabela.setPrimaryKey("sequencia");
				tblTabela.setOrder("datacons desc, horacons desc, sequencia desc");
				
				tblTabela.addIndex("datacons_horacons_sequencia", "datacons, horacons, sequencia");
				tblTabela.addIndex("codiasso", "codiasso");
				tblTabela.addIndex("codiespe_datacons_horacons", "codiespe, datacons, horacons");
				tblTabela.addIndex("codimedi_datacons_horacons", "codimedi, datacons, horacons");
				tblTabela.addIndex("cortesia_datacons_horacons", "cortesia, datacons, horacons");
				tblTabela.addIndex("tipoatend_datacons_horacons",  "tipoatend, datacons, horacons" );
				tblTabela.addIndex("uidcarrcomp_sequencia", "uidcarrcomp, sequencia");
				
				tblTabela.addJoin("especialidades", "codiespe", "codiespe");
				tblTabela.fieldByName("desccodiespe").setExpression("especialidades.descricao");
				
				tblTabela.addJoin("medicos", "codimedi", "codimedi");
				tblTabela.fieldByName("desccodimedi").setExpression("medicos.nome");
				
				tblTabela.addInitialValueEventListener(new InitialValueEventListener() {
					@Override
					public void onInitialValue(InitialValueEvent event) {
						event.getTable().setValue("tipoatend", "C");
						event.getTable().setValue("desctipoatend", "CONSULTA");
						event.getTable().setDate("ulticons", null);
						event.getTable().setInteger("ulticonsdias", null);
						event.getTable().setInteger("codimedi", null);
						event.getTable().setString("desccodimedi", "");
						event.getTable().setString("cortesia", "N");
						event.getTable().setString("atendido", "S");
					}
				});
				
				tblTabela.addAfterInsertEventListener(new AfterInsertEventListener() {
					@Override
					public void onAfterInsert(AfterInsertEvent event) {
						// Na inclusão de uma consulta, deve permitir que seja selecionado somente especialidades
						// que permintam marcacao de consulta, como ecg e clinico geral e pediatria
						ComboBox cmbCodiEspe = event.getTable().fieldByName("codiespe").getComboBox();
						cmbCodiEspe.setWhere("(especialidades.permagencons='S')");
					}
				});

				tblTabela.addAfterUpdateEventListener(new AfterUpdateEventListener() {
					@Override
					public void onAfterUpdate(AfterUpdateEvent event) {
						//ComboBox cmbCodiEspe = event.getTable().fieldByName("codiespe").getComboBox();
						//cmbCodiEspe.setWhere("(especialidades.permagencons='S')");
					}
				});
				
				
				tblTabela.addAfterFilterEventListener(new AfterFilterEventListener() {
					@Override
					public void onAfterFilter(AfterFilterEvent event) {
						ComboBox cmbCodiEspe = event.getTable().fieldByName("codiespe").getComboBox();
						cmbCodiEspe.setWhere(null);
					}
				});
				
				Field field = tblTabela.fieldByName("sequencia");
				field.setAutoIncrement(true);
				field.setReadOnly(true);
				
				InternalSearch internalSearch = tblTabela.fieldByName("tipoatend").addInternalSearch();
				internalSearch.addItem("C", "CONSULTA");
				internalSearch.addItem("R", "RETORNO");
				internalSearch.addItem("E", "EXAME");
				
				InternalSearch isUltiTipoAtend = tblTabela.fieldByName("ultitipoatend").addInternalSearch();
				isUltiTipoAtend.addItem("C", "CONSULTA");
				isUltiTipoAtend.addItem("R", "RETORNO");
				isUltiTipoAtend.addItem("E", "EXAME");
				
				tblTabela.fieldByName("desctipoatend").setValueOfInternalSearch("tipoatend");
				tblTabela.fieldByName("ultidesctipoatend").setValueOfInternalSearch("ultitipoatend");
				
				//tblTabela.fieldByName("codiasso").setReadOnly(true);
				tblTabela.fieldByName("nome").setReadOnly(true);
				tblTabela.fieldByName("diasema").setReadOnly(true);
				tblTabela.fieldByName("datacons").setReadOnly(true);
				tblTabela.fieldByName("horacons").setReadOnly(true);
				
				tblTabela.fieldByName("ulticons").setReadOnly(true);
				tblTabela.fieldByName("ulticonsdias").setReadOnly(true);
				tblTabela.fieldByName("ultitipoatend").setReadOnly(true);
				tblTabela.fieldByName("ultidesctipoatend").setReadOnly(true);
				tblTabela.fieldByName("ultiatendsequ").setReadOnly(true);
				
				tblTabela.fieldByName("desccodiespe").setReadOnly(true);
				tblTabela.fieldByName("desctipoatend").setReadOnly(true);
				tblTabela.fieldByName("desccodimedi").setReadOnly(true);
				
				tblTabela.fieldByName("codiproc01_valor").setReadOnly(true);
				tblTabela.fieldByName("codiproc02_valor").setReadOnly(true);
				tblTabela.fieldByName("codiproc03_valor").setReadOnly(true);
				tblTabela.fieldByName("codiproc04_valor").setReadOnly(true);
				tblTabela.fieldByName("codiproc05_valor").setReadOnly(true);
				tblTabela.fieldByName("codiproc06_valor").setReadOnly(true);
				tblTabela.fieldByName("codiproc07_valor").setReadOnly(true);
				tblTabela.fieldByName("codiproc08_valor").setReadOnly(true);
				tblTabela.fieldByName("codiproc09_valor").setReadOnly(true);
				tblTabela.fieldByName("codiproc10_valor").setReadOnly(true);
				tblTabela.fieldByName("codiproc11_valor").setReadOnly(true);
				tblTabela.fieldByName("codiproc12_valor").setReadOnly(true);
				tblTabela.fieldByName("codiproc13_valor").setReadOnly(true);
				tblTabela.fieldByName("codiproc14_valor").setReadOnly(true);
				tblTabela.fieldByName("codiproc15_valor").setReadOnly(true);
				tblTabela.fieldByName("codiproc16_valor").setReadOnly(true);
				tblTabela.fieldByName("codiproc17_valor").setReadOnly(true);
				tblTabela.fieldByName("codiproc18_valor").setReadOnly(true);
				tblTabela.fieldByName("codiproc19_valor").setReadOnly(true);
				tblTabela.fieldByName("codiproc20_valor").setReadOnly(true);
				tblTabela.fieldByName("codiproc21_valor").setReadOnly(true);
				tblTabela.fieldByName("codiproc22_valor").setReadOnly(true);
				tblTabela.fieldByName("codiproc23_valor").setReadOnly(true);
				tblTabela.fieldByName("codiproc24_valor").setReadOnly(true);
				tblTabela.fieldByName("codiproc25_valor").setReadOnly(true);
				tblTabela.fieldByName("codiproc26_valor").setReadOnly(true);
				tblTabela.fieldByName("codiproc27_valor").setReadOnly(true);
				tblTabela.fieldByName("codiproc28_valor").setReadOnly(true);
				tblTabela.fieldByName("codiproc29_valor").setReadOnly(true);
				tblTabela.fieldByName("codiproc30_valor").setReadOnly(true);
				tblTabela.fieldByName("codiproc31_valor").setReadOnly(true);
				tblTabela.fieldByName("codiproc32_valor").setReadOnly(true);
				
				tblTabela.fieldByName("vlrtotal").setReadOnly(true);
				tblTabela.fieldByName("vlrcustototal").setReadOnly(true);
				
				tblTabela.fieldByName("codiasso").setRequired(true);
				tblTabela.fieldByName("nome").setRequired(true);
				tblTabela.fieldByName("paciente").setRequired(true);
				tblTabela.fieldByName("nasc").setRequired(true);
				tblTabela.fieldByName("cortesia").setRequired(true);
				tblTabela.fieldByName("codiespe").setRequired(true);
				tblTabela.fieldByName("tipoatend").setRequired(true);
				tblTabela.fieldByName("atendido").setRequired(true);
				//tblTabela.fieldByName("datacons").setRequired(true);
				//tblTabela.fieldByName("horacons").setRequired(true);
				//tblTabela.fieldByName("codimedi").setRequired(true);
				//tblTabela.fieldByName("observacoes").setRequired(true);

				internalSearch = new InternalSearch();
				internalSearch.addItem("S", "SIM");
				internalSearch.addItem("N", "NÃO");
				tblTabela.fieldByName("cortesia").setInternalSearch(internalSearch);
				
				ComboBox cbUidUsuaLibe = tblTabela.fieldByName("uidusualibe").addComboBox("usuarios", "nome", "nome", "uid");
				
				internalSearch = new InternalSearch();
				internalSearch.addItem("C", "CONSULTA");
				internalSearch.addItem("R", "RETORNO");
				internalSearch.addItem("E", "EXAME");
				tblTabela.fieldByName("tipoatend").setInternalSearch(internalSearch);

				internalSearch = new InternalSearch();
				internalSearch.addItem("S", "SIM");
				internalSearch.addItem("N", "NÃO");
				tblTabela.fieldByName("atendido").setInternalSearch(internalSearch);
				
				ComboBox comboBoxAssociado = tblTabela.fieldByName("codiasso").addComboBox("associados", "nome", "nome", "sequencia");
				comboBoxAssociado.setAdditionalFieldsToLoad("nasc");
				comboBoxAssociado.addComboBoxItemCaptionGeneratorEventListener(new ComboBoxItemCaptionGeneratorEventListener() {
					@Override
					public void onComboBoxItemCaptionGenerator(ComboBoxItemCaptionGeneratorEvent event) {
						String retorno = "";
						
						if (!event.getSimpleRecord().getString("nome").isEmpty()) {
							retorno = event.getSimpleRecord().getString("nome");
						}
						
						if ((event.getSimpleRecord().fieldExists("nasc")) && (event.getSimpleRecord().getDate("nasc")!=null)) {
							retorno += " - Nasc: " + Utils.simpleDateFormat(event.getSimpleRecord().getDate("nasc"));
						}
						
						event.setItemCaption(retorno);
					}
				});

				ExistenceCheck existenceCheck = tblTabela.fieldByName("codiasso").addExistenceCheck("associados", "sequencia", "codiasso", "Associado informado é inválidp!");
				existenceCheck.addAfterExistenceCheckEventListener(new AfterExistenceCheckEventListener() {
					@Override
					public void onAfterExistenceCheck(AfterExistenceCheckEvent event) {
						event.getSourceTable().setValue("nome", event.getTargetTable().getString("nome"));
						if (event.getSourceTable().getString("paciente").isEmpty()) {
							event.getSourceTable().setValue("paciente", event.getTargetTable().getString("nome"));
							event.getSourceTable().setValue("nasc", event.getTargetTable().getDate("nasc"));
						}
					}
				});
				
				ComboBox cmbCodiEspe = tblTabela.fieldByName("codiespe").addComboBox("especialidades", "descricao", "descricao", "codiespe");
				
				existenceCheck = tblTabela.fieldByName("codiespe").addExistenceCheck("especialidades", "codiespe", "codiespe", "Especialidade informada é inválida!");
				existenceCheck.addAfterExistenceCheckEventListener(new AfterExistenceCheckEventListener() {
					@Override
					public void onAfterExistenceCheck(AfterExistenceCheckEvent event) {
						event.getSourceTable().setValue("desccodiespe", event.getTargetTable().getString("descricao"));
						event.getSourceTable().setValue("codiespe_valor", event.getTargetTable().getDouble("valor"));
						event.getSourceTable().setValue("codiespe_vlrcusto", event.getTargetTable().getDouble("vlrcusto"));
					}
				});
				
				tblTabela.fieldByName("codiespe").addAfterValidateOnEditEventListener(new AfterValidateOnEditEventListener() {
					@Override
					public void onAfterValidateOnEdit(AfterValidateOnEditEvent event) {
						// Deve ser melhorado levando em conta a data da consulta e uma modificacao no registro
						String vcodiasso = event.getTable().getString("codiasso");
						String vpaciente = event.getTable().getString("paciente");
						String vcodiespe = event.getTable().getString("codiespe");
	
						// Esse codigo deve ser executado somente se o conteudo de codiespe for alterado, com a finalidade
						// de atualizar os dados da ultima consulta da especialidade selecionada
						String comando ="select marccons.datacons, marccons.tipoatend, marccons.codimedi, medicos.nome from marccons";
						comando += " left join medicos on (medicos.codimedi=marccons.codimedi)";
						comando += "where (codiasso=" + vcodiasso + ") and (paciente='" + vpaciente + "') and (codiespe="+vcodiespe+")";
						comando += "order by datacons desc, sequencia desc limit 1";
						
						try {
							event.getTable().getDatabase().openConnection();
							ResultSet resultSet = event.getTable().getDatabase().executeSelect(comando);
							if (resultSet.next()) {
								// Atualiza dados da consulta
								event.getTable().setValue("ulticons", resultSet.getDate("datacons"));
								int dias = (int) Utils.getDateDiff(resultSet.getDate("datacons"), new Date(), TimeUnit.DAYS);
								event.getTable().setValue("ulticonsdias", dias);
								if ((dias<=20) && (event.getTable().getString("tipoatend").equalsIgnoreCase("C"))) {
									event.getTable().setValue("tipoatend", "R");
									event.getTable().setValue("desctipoatend", "RETORNO");
								}
								
								// Atualiza dados do médico
								Integer vcodimedi = resultSet.getInt("codimedi");
								comando = "select codimedi, nome, situacao from medicos";
								comando += " where (medicos.codimedi=" + vcodimedi + ")";
								resultSet = event.getTable().getDatabase().executeSelect(comando);
								if (resultSet.next()) {
									// Caso o medico esteja ativo
									if (resultSet.getString("situacao").equalsIgnoreCase("A")) {
										event.getTable().setValue("codimedi", vcodimedi);
										event.getTable().setValue("desccodimedi", resultSet.getString("nome"));
									}
								}
							}
							else {
								event.getTable().setLoadingDataToForm(true);
								event.getTable().setDate("ulticons", null);
								event.getTable().setInteger("ulticonsdias", null);
								event.getTable().setValue("tipoatend", "C");
								event.getTable().setValue("desctipoatend", "CONSULTA");
								event.getTable().setInteger("codimedi", 0);
								event.getTable().setString("desccodimedi", "");
								event.getTable().setDate("datacons", null);
								event.getTable().setString("horacons", "");
								event.getTable().setLoadingDataToForm(false);
							}
						}
						catch (Exception e) {
							System.out.println(e.getMessage());
						}
						finally {
							event.getTable().getDatabase().closeConnection();
						}
					}
				});
				
				/**/
				
				ComboBox comboBox = tblTabela.fieldByName("codimedi").addComboBox("medicos", "nome", "nome", "codimedi");
				tblTabela.fieldByName("codimedi").setReadOnly(true);
				
				ForeingSearch foreingSearch2 = tblTabela.fieldByName("codimedi").addForeingSearch();
				foreingSearch2.setTargetRmGridName("medicos");
				foreingSearch2.setTargetIndexName("codimedi");
				foreingSearch2.setRelationship("codimedi");
				foreingSearch2.setReturnFieldName("codimedi");
				foreingSearch2.setTitle("Pesquisa por MÉDICOS:");
				foreingSearch2.setOrder("nome");
				foreingSearch2.setAutoOpenFilterForm(false);
				foreingSearch2.setWidth("800px");
				foreingSearch2.setHeight("655px");
				foreingSearch2.addBeforeForeingSearchEventListener(new BeforeForeingSearchEventListener() {
					@Override
					public void onBeforeForeingSearch(BeforeForeingSearchEvent event) {
						Table tableTarget = event.getTargetTable();
						tableTarget.setWhere("exists (select * from espemedi where (espemedi.codiespe="+event.getSourceTable().getInteger("codiespe")+") and (espemedi.codimedi=medicos.codimedi))");
					}
				});
				
				existenceCheck = tblTabela.fieldByName("codimedi").addExistenceCheck("medicos", "codimedi", "codimedi", "Médico informada é inválida!");
				existenceCheck.addAfterExistenceCheckEventListener(new AfterExistenceCheckEventListener() {
					@Override
					public void onAfterExistenceCheck(AfterExistenceCheckEvent event) {
						event.getSourceTable().setValue("desccodimedi", event.getTargetTable().getString("nome"));
					}
				});
				
				tblTabela.addSpecialConditionOfDeleteEventListener(new SpecialConditionOfDeleteEventListener() {
					@Override
					public void onSpecialConditionOfDelete(SpecialConditionOfDeleteEvent event) {
						event.setValid(false);
						event.setErrorMessage("nao pode nao pode nao pode");
					}
				});
			}
		});
		
	    RmGridRegistry rmGridRegistry = database.addRmGridRegistry("marccons");
	    rmGridRegistry.addLoadRmGridEventListener(new LoadRmGridEventListener() {
			@Override
			public void onLoadRmGrid(LoadRmGridEvent event) {
				Table table = event.getTable();
				if (table==null) {
					table = database.loadTableByName("marccons");
				}
				
				RmGrid rmGrid = event.getRmGrid();
				rmGrid.setTable(table);
				rmGrid.setHeight("550px");
				rmGrid.setLimit(10);
				rmGrid.addField("sequencia", "Sequência", 100D);
				rmGrid.addField("datacons", "Data do atendimento", 150d);
				rmGrid.addField("horacons", "Hora", 80d);
				rmGrid.addField("nome", "Nome do associado", 250d, 1);
				rmGrid.addField("paciente", "Paciente", 250d);
				rmGrid.addField("desccodiespe", "Especialidade", 180d);
				rmGrid.addField("desctipoatend", "Atendimento", 130d);
				rmGrid.addField("incluser", "Inclusão - Usuário", 150d);
				rmGrid.addField("incldata", "Inclusão - Data", 130d);
				rmGrid.addField("modiuser", "Modificação - Usuário", 150d);
				rmGrid.addField("modidata", "Modificação - Data", 130d);
				
				rmGrid.setAllowDelete(false);
				
				ControlForm controlForm = rmGrid.getForm();
				controlForm.setTitle("Manutenção de associados:");
				controlForm.setWidth(1100d);
				controlForm.setHeight(600d);
				
				controlForm.addNewLine();
				controlForm.addSection("cadastro", "Guida de atendimento de Consulta/Exames:", SectionState.MAXIMIZED);
				{
					controlForm.addNewLine();
					controlForm.addField("sequencia", "Sequência", 90d);
					controlForm.addField("codiasso", "Cód.Associado", 130d, 1);
					//controlForm.addField("nome", "Nome", 250d, 1);
					
					controlForm.addNewLine();
					controlForm.addField("paciente", "Paciente", 250d, 1);
					controlForm.addField("nasc", "Nascimento", 150d);
					
					controlForm.addNewLine();
					controlForm.addField("codiespe", "Especialidade", 120d, 1);
					controlForm.addField("ulticons", "Último atendimento", 150d);
					controlForm.addField("ulticonsdias", "Dias", 50d);
					controlForm.addField("ultiatendsequ", "Última sequência", 150d);
					controlForm.addField("ultidesctipoatend", "Atendimento", 110d);
					
					controlForm.addNewLine();
					controlForm.addField("codimedi", "Médico", 120d, 1);
					//controlForm.addField("desccodimedi", "Médico", 200d, 1);
					
					ControlButton controlButton = controlForm.addButton("Agenda");
					controlButton.addControlButtonClickEventListener(new ControlButtonClickEventListener() {
						@Override
						public void onControlButtonClick(ControlButtonClickEvent event) {
							if (event.getRmForm().getTable().fieldByName("codiespe").validate()) {
								Integer codigoEspecialidade = event.getRmForm().getTable().getInteger("codiespe");
								Integer codigoMedico = event.getRmForm().getTable().getInteger("codimedi");
								Agenda2View agenda2View = new Agenda2View(codigoEspecialidade, codigoMedico, false);
								
								RmFormWindow formWindow = new RmFormWindow();
								formWindow.setTitle("Agendamento de consultas:");
								formWindow.setWidth("1100px");
								formWindow.setHeight("600px");
								formWindow.getBody().addComponent(agenda2View);
								agenda2View.setRmFormWindow(formWindow);
								agenda2View.setTable(event.getRmForm().getTable());
								
								RmFormButtonBase buttonBase = formWindow.addCancelButton();
								buttonBase.addRmFormButtonClickEventListener(new RmFormButtonClickEventListener() {
									@Override
									public void onRmFormButtonClick(RmFormButtonClickEvent event) {
										RmFormWindow window = (RmFormWindow) event.getWindow();
										window.close();
									}
								});
								
								buttonBase = formWindow.addSaveButton();
								buttonBase.addRmFormButtonClickEventListener(new RmFormButtonClickEventListener() {
									@Override
									public void onRmFormButtonClick(RmFormButtonClickEvent event) {
										System.out.println(agenda2View.getDataSelecionadaAgenda());
									}
								});

								formWindow.show();
							}
						}
					});
					
					controlForm.addNewLine();
					controlForm.addField("datacons", "Data da consulta", 150d);
					controlForm.addField("diasema", "Dia da semana", 100d, 1);
					controlForm.addField("horacons", "Hora da consulta", 150d);
					controlForm.addField("desctipoatend", "Atendimento", 110d);
					controlForm.addField("atendido", "Atendido", 100d);
					
					controlForm.addField("cortesia", "Cortesia", 100d);
					controlForm.addField("uidusualibe", "Autorização", 300d);
					
					controlForm.addNewLine();
					controlForm.addField("observacoes", "Observacoes", 100d, 300d, 1);
					
					//apos pesquisa de existencia de especialidade atualiza
					//valor, vlrcusto
					controlForm.exitSection();
					controlForm.addNewLine();
					controlForm.addSection("medicoes", "Medições do estado de saúde do paciente:", SectionState.MAXIMIZED);
					{
						controlForm.addNewLine();
						controlForm.addField("altura", "Altura", 100d, 1);
						controlForm.addField("peso", "Peso", 100d, 1);
						controlForm.addField("pressao", "Pressão", 100d, 1);
						controlForm.addField("pulsacao", "Pulsação", 100d, 1);
						controlForm.addField("temperatura", "Temperatura", 100d, 1);
					}
				}
				
				//*********************************************************************************************
				controlForm = rmGrid.getFormFilter();
				controlForm.setTitle("Pesquisa por associados:");
				controlForm.setWidth(1100d);
				controlForm.setHeight(600d);
				
				controlForm.addNewLine();
				controlForm.addSection("cadastro", "Guida de atendimento de Consulta/Exames:", SectionState.MAXIMIZED);
				{
					controlForm.addNewLine();
					controlForm.addField("sequencia", "Sequência", 90d);
					controlForm.addField("codiasso", "Cód.Associado", 130d, 1);
					//controlForm.addField("nome", "Nome", 250d, 1);
					
					controlForm.addNewLine();
					controlForm.addField("paciente", "Paciente", 250d, 1);
					controlForm.addField("nasc", "Nascimento", 150d);
					
					controlForm.addNewLine();
					controlForm.addField("codiespe", "Especialidade", 120d, 1);
					//controlForm.addField("desccodiespe", "Especialidade", 200d, 1);
					controlForm.addField("ulticons", "Última consulta", 150d);
					controlForm.addField("ulticonsdias", "Dias", 50d);
					controlForm.addField("desctipoatend", "Atendimento", 110d);
					
					controlForm.addNewLine();
					controlForm.addField("codimedi", "Médico", 120d, 1);
					//controlForm.addField("desccodimedi", "Médico", 200d, 1);
					
					controlForm.addNewLine();
					controlForm.addField("datacons", "Data da consulta", 150d);
					controlForm.addField("diasema", "Dia da semana", 100d, 1);
					controlForm.addField("horacons", "Hora da consulta", 150d);
					controlForm.addField("atendido", "Atendido", 100d);
					
					controlForm.addField("cortesia", "Cortesia", 100d);
					controlForm.addField("uidusualibe", "Autorização", 300d);
					
					controlForm.addNewLine();
					controlForm.addField("observacoes", "Observacoes", 100d, 300d, 1);
					
					//apos pesquisa de existencia de especialidade atualiza
					//valor, vlrcusto
				}
				
				RmFormButtonBase button = controlForm.addRmFormButton("Emitir guia de atendimento");
				button.addRmFormButtonClickEventListener(new RmFormButtonClickEventListener() {
					@Override
					public void onRmFormButtonClick(RmFormButtonClickEvent event) {
						
						if (event.getControlForm().getTable().validate()) {
							event.getControlForm().getTable().execute();
							
							if (!event.getControlForm().getTable().getString("uid").isEmpty()) {
								Table tblParametros = event.getControlForm().getTable().getDatabase().loadTableByName("parametros");
								tblParametros.select("*");
								tblParametros.loadData();
								
								String url = "http://" + tblParametros.getString("reportServer") + "/php-app/fichaconsulta2.php?recordId="+event.getControlForm().getTable().getInteger("sequencia");
								UI.getCurrent().getPage().open(url, "_blank", false);
							}
						}
						else {
							System.out.println("não validou o conteudo do formulario");
						}
					}
				});
				
				RmFormButtonBase button2 = controlForm.addRmFormButton("Agendar retorno");
				button2.addRmFormButtonClickEventListener(new RmFormButtonClickEventListener() {
					@Override
					public void onRmFormButtonClick(RmFormButtonClickEvent event) {
						if (event.getControlForm().getTable().validate()) {
							event.getControlForm().getTable().execute();
						}
						else {
							System.out.println("não validou o conteudo do formulario");
						}
					}
				});
			}
		});
		
		CommandRegistry commandRegistry = database.addCommandRegistry("marccons");
		commandRegistry.addCommandEventListener(new CommandEventListener() {
			@Override
			public void onCommandExecute(CommandEvent event) {
				RmGrid rmGrid = database.loadRmGridByName("marccons");
				rmGrid.updateContent();
				
				ApplicationUI ui = (ApplicationUI) UI.getCurrent();
				ui.getSystemView().addTab(rmGrid, "Consultas", true);
			}
		});
	}
}
