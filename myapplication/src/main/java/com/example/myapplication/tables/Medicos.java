package com.example.myapplication.tables;

import com.evolucao.rmlibrary.database.Database;
import com.evolucao.rmlibrary.database.InternalSearch;
import com.evolucao.rmlibrary.database.RmGridRegistry;
import com.evolucao.rmlibrary.database.Table;
import com.evolucao.rmlibrary.database.TableRegistry;
import com.evolucao.rmlibrary.database.enumerators.FieldType;
import com.evolucao.rmlibrary.database.events.CommandEvent;
import com.evolucao.rmlibrary.database.events.LoadRmGridEvent;
import com.evolucao.rmlibrary.database.events.LoadTableEvent;
import com.evolucao.rmlibrary.database.events.CommandEvent.CommandEventListener;
import com.evolucao.rmlibrary.database.events.LoadRmGridEvent.LoadRmGridEventListener;
import com.evolucao.rmlibrary.database.events.LoadTableEvent.LoadTableEventListener;
import com.evolucao.rmlibrary.database.registry.CommandRegistry;
import com.evolucao.rmlibrary.ui.controller.ControlForm;
import com.evolucao.rmlibrary.ui.form.enumerators.SectionState;
import com.evolucao.rmlibrary.ui.production.RmGrid;
import com.evolucao.weblibrary.ApplicationUI;
import com.vaadin.ui.UI;

public class Medicos {
	public static void configure(Database database) {
		TableRegistry tableRegistry = database.addTableRegistry("medicos");
		tableRegistry.addLoadTableEventListener(new LoadTableEventListener() {
			@Override
			public void onLoadTable(LoadTableEvent event) {
				Table tblTabela = event.getTable();
				tblTabela.setTableName("medicos");
				tblTabela.addField("codimedi", FieldType.INTEGER, 10);
				tblTabela.addField("nome", FieldType.VARCHAR, 50);
				tblTabela.addField("situacao", FieldType.VARCHAR, 1);
				tblTabela.addField("tipotota", FieldType.VARCHAR, 1);
				tblTabela.setPrimaryKey("codimedi");
				tblTabela.setOrder("nome");
				
				tblTabela.addIndex("codimedi", "codimedi");
				
				tblTabela.setRequired("nome", true);
				tblTabela.setRequired("situacao", true);
				tblTabela.setRequired("tipotota", true);
				
				//tblTabela.fieldByName("codimedi").setReadOnly(true);
				tblTabela.fieldByName("codimedi").setReadOnlyUpdate(true);
				
				InternalSearch internalSearch = new InternalSearch();
				internalSearch.addItem("A", "Ativo");
				internalSearch.addItem("D", "Destativo");
				tblTabela.fieldByName("situacao").setInternalSearch(internalSearch);
				
				InternalSearch internalSearchTipoTota = new InternalSearch();
				internalSearchTipoTota.addItem("1", "Número de atendimentos");
				internalSearchTipoTota.addItem("2", "Número de procedimentos");
				tblTabela.fieldByName("tipotota").setInternalSearch(internalSearchTipoTota);
				
				Table tblEspeMedi = database.loadTableByName("espemedi");
				tblTabela.addTableChild(tblEspeMedi, "uidmedico", true);
				
				Table tblAgenda = database.loadTableByName("agenda");
				tblTabela.addTableChild(tblAgenda, "uidmedico", true);
			}
		});
		
	    RmGridRegistry rmGridRegistry = database.addRmGridRegistry("medicos");
	    rmGridRegistry.addLoadRmGridEventListener(new LoadRmGridEventListener() {
			@Override
			public void onLoadRmGrid(LoadRmGridEvent event) {
				Table table = event.getTable();
				if (table==null) {
					table = database.loadTableByName("medicos");
				}
				
				RmGrid rmGrid = event.getRmGrid();
				rmGrid.setTable(table);
				rmGrid.setLimit(10);
				rmGrid.addField("codimedi", "Cód.Medico", 110d);
				rmGrid.addField("nome", "Nome do médico", 100d, 1);
				rmGrid.addField("situacao", "Situação", 100d);
				
				ControlForm controlForm = rmGrid.getForm();
				controlForm.setTitle("Manutenção de médicos:");
				controlForm.setWidth(800d);
				controlForm.setHeight(800d);
				
				controlForm.addNewLine();
				controlForm.addSection("cadastro", "Dados do médico:", SectionState.MAXIMIZED);
				{
					controlForm.addNewLine();
					controlForm.addField("codimedi", "Cód.Medico", 100d);
					controlForm.addField("nome", "Nome do médico", 100d, 1);
					
					controlForm.addNewLine();
					controlForm.addField("situacao", "Situação", 100d, 1);
					controlForm.addField("tipotota", "Tipo de totalização", 100d, 1);
				}
				
				controlForm.exitSection();
				controlForm.addNewLine();
				controlForm.addSection("especialidades", "Especialidades que o médico atende:", SectionState.MAXIMIZED, 400d);
				{
					controlForm.addRmGrid("espemedi");
				}
				
				controlForm.exitSection();
				controlForm.addNewLine();
				controlForm.addSection("agenda", "Programação da agenda de atendimento:", SectionState.MAXIMIZED, 450d);
				{
					controlForm.addRmGrid("agenda");
				}
				
				
				controlForm = rmGrid.getFormFilter();
				controlForm.setTitle("Pesquisa por Médicos:");
				controlForm.setWidth(600d);
				controlForm.setHeight(450d);
				
				controlForm.addNewLine();
				controlForm.addSection("cadastro", "Manutenção de médicos:", SectionState.MAXIMIZED);
				{
					controlForm.addNewLine();
					controlForm.addField("codimedi", "Cód.Medico", 100d);
					controlForm.addField("nome", "Nome do médico", 100d, 1);
					
					controlForm.addNewLine();
					controlForm.addField("situacao", "Situação", 100d, 1);
					controlForm.addField("tipotota", "Tipo de totalização", 100d, 1);
				}
			}
		});
	    
		CommandRegistry commandRegistry = database.addCommandRegistry("medicos");
		commandRegistry.addCommandEventListener(new CommandEventListener() {
			@Override
			public void onCommandExecute(CommandEvent event) {
				RmGrid rmGrid = database.loadRmGridByName("medicos");
				rmGrid.updateContent();
				
				ApplicationUI ui = (ApplicationUI) UI.getCurrent();
				ui.getSystemView().addTab(rmGrid, "Médicos", true);
			}
		});
	}
}
