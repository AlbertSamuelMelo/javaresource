package com.example.myapplication.tables;

import com.evolucao.rmlibrary.database.Database;
import com.evolucao.rmlibrary.database.RmGridRegistry;
import com.evolucao.rmlibrary.database.Table;
import com.evolucao.rmlibrary.database.TableRegistry;
import com.evolucao.rmlibrary.database.enumerators.FieldType;
import com.evolucao.rmlibrary.database.events.CommandEvent;
import com.evolucao.rmlibrary.database.events.LoadRmGridEvent;
import com.evolucao.rmlibrary.database.events.LoadTableEvent;
import com.evolucao.rmlibrary.database.events.CommandEvent.CommandEventListener;
import com.evolucao.rmlibrary.database.events.LoadRmGridEvent.LoadRmGridEventListener;
import com.evolucao.rmlibrary.database.events.LoadTableEvent.LoadTableEventListener;
import com.evolucao.rmlibrary.database.registry.CommandRegistry;
import com.evolucao.rmlibrary.ui.controller.ControlForm;
import com.evolucao.rmlibrary.ui.form.enumerators.SectionState;
import com.evolucao.rmlibrary.ui.production.RmGrid;
import com.evolucao.weblibrary.ApplicationUI;
import com.vaadin.ui.UI;

public class TabelaConversao {
	public static void configure(Database database) {
		TableRegistry tableRegistry = database.addTableRegistry("tabelaconversao");
		tableRegistry.addLoadTableEventListener(new LoadTableEventListener() {
			@Override
			public void onLoadTable(LoadTableEvent event) {
				Table table = event.getTable();
				table.setTableName("tabelaconversao");
				table.addField("codicont", FieldType.VARCHAR, 50);
				table.addField("codiresu", FieldType.VARCHAR, 10);
				table.addField("descricao", FieldType.VARCHAR, 50);
				table.setPrimaryKey("codicont");
				table.setOrder("descricao");
				
				table.addIndex("codicont", "conta");
				table.addIndex("codiresu", "contresu");
				table.addIndex("descricao", "descricao");
			}
		});
		
	    RmGridRegistry rmGridRegistry = database.addRmGridRegistry("tabelaconversao");
	    rmGridRegistry.addLoadRmGridEventListener(new LoadRmGridEventListener() {
			@Override
			public void onLoadRmGrid(LoadRmGridEvent event) {
				Table table = event.getTable();
				if (table==null) {
					table = database.loadTableByName("tabelaconversao");
				}
				
				RmGrid rmGrid = event.getRmGrid();
				rmGrid.setTable(table);
				rmGrid.setLimit(10);
				rmGrid.addField("codicont", "Conta contábil", 150d);
				rmGrid.addField("codiresu", "Cód.resumido", 150d);
				rmGrid.addField("descricao", "Descricao", 100d, 1);
				
				ControlForm controlForm = rmGrid.getForm();
				controlForm.setTitle("Manutenção de contas contábeis:");
				controlForm.setWidth(800d);
				controlForm.setHeight(300d);
				
				controlForm.addNewLine();
				controlForm.addSection("caixas", "Controle de contas contábeis.", SectionState.MAXIMIZED);
				{
					controlForm.addNewLine();
					controlForm.addField("codicont", "Código contábil", 150d);
					controlForm.addField("codiresu", "Código resumido", 150d);
					controlForm.addField("descricao", "Descrição", 100d, 1);
				}
				
				controlForm = rmGrid.getFormFilter();
				controlForm.setTitle("Pesquisa por contas contábeis:");
				controlForm.setWidth(800d);
				controlForm.setHeight(300d);
				
				controlForm.addNewLine();
				controlForm.addSection("caixas", "Controle de contas contábeis.", SectionState.MAXIMIZED);
				{
					controlForm.addNewLine();
					controlForm.addField("codicont", "Código contábil", 150d);
					controlForm.addField("codiresu", "Código resumido", 150d);
					controlForm.addField("descricao", "Descrição", 100d, 1);
				}
			}
		});
	    
		CommandRegistry commandRegistry = database.addCommandRegistry("tabelaconversao");
		commandRegistry.addCommandEventListener(new CommandEventListener() {
			@Override
			public void onCommandExecute(CommandEvent event) {
				RmGrid rmGrid = database.loadRmGridByName("tabelaconversao");
				rmGrid.updateContent();

				ApplicationUI ui = (ApplicationUI) UI.getCurrent();
				ui.getSystemView().addTab(rmGrid, "Contas contábeis", true);
			}
		});
	}
}
