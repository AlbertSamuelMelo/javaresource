package com.example.myapplication.rotinas;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.evolucao.rmlibrary.database.SimpleRecord;
import com.evolucao.rmlibrary.database.Table;
import com.evolucao.rmlibrary.database.enumerators.MessageWindowType;
import com.evolucao.rmlibrary.utils.Utils;
import com.evolucao.rmlibrary.window.RmFormButtonBase;
import com.evolucao.rmlibrary.window.RmFormWindow;
import com.evolucao.rmlibrary.window.event.RmFormButtonClickEvent;
import com.evolucao.rmlibrary.window.event.RmFormButtonClickEvent.RmFormButtonClickEventListener;
import com.evolucao.weblibrary.ApplicationUI;
import com.mysql.fabric.xmlrpc.base.Array;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.FinishedEvent;
import com.vaadin.ui.Upload.FinishedListener;
import com.vaadin.ui.VerticalLayout;

public class ConvArquivosProcess extends VerticalLayout {
	private List<String> arrayLinha = new ArrayList<String>();
	private int lineCounter = 0;
	private double allMoneyFound = 0;

	public ConvArquivosProcess() {
    	LineBreakCounter lineBreakCounter = new LineBreakCounter();
    	
        final VerticalLayout layout = new VerticalLayout();
        addComponent(layout);
        
        Upload sample = new Upload(null, lineBreakCounter);
        sample.setImmediateMode(false);
        sample.setButtonCaption("Upload File");
        sample.setSizeFull();
        
        UploadInfoWindow uploadInfoWindow = new UploadInfoWindow(sample, lineBreakCounter);
        
        sample.addStartedListener(event -> {
        	lineBreakCounter.arquivo = new ArrayList<String>();
        	lineBreakCounter.linha = "";
            if (uploadInfoWindow.getParent() == null) {
                UI.getCurrent().addWindow(uploadInfoWindow);
            }
            uploadInfoWindow.setClosable(false);
        });
        
        
        //sample.addFinishedListener(event -> uploadInfoWindow.setClosable(true));
        
        sample.addFinishedListener(new FinishedListener() {
			public void uploadFinished(FinishedEvent event) {
				lineBreakCounter.arquivo.add(lineBreakCounter.linha);
				uploadInfoWindow.setClosable(true);
				
				String filePath = "";
				
				String os = System.getProperty("os.name");
				
				System.out.println(os);
				
				String mes = "00";
				
				String atu = lineBreakCounter.getFilename();
				
				if (atu.contains("jan")) {
					mes = "01";
				} else if (atu.contains("fev")) {
					mes = "02";
				} else if (atu.contains("mar")) {
					mes = "03";
				} else if (atu.contains("abr")) {
					mes = "04";
				} else if (atu.contains("mai")) {
					mes = "05";
				} else if (atu.contains("jun")) {
					mes = "06";
				} else if (atu.contains("jul")) {
					mes = "07";
				} else if (atu.contains("ago")) {
					mes = "08";
				} else if (atu.contains("set")) {
					mes = "09";
				} else if (atu.contains("out")) {
					mes = "10";
				} else if (atu.contains("nov")) {
					mes = "11";
				} else if (atu.contains("dez")) {
					mes = "12";
				}
					
				
				if (os.contains("Windows")) {
					//filePath = Paths.get(".").toAbsolutePath().normalize().toString() + "\\banco-de-dados\\";
					filePath = "c:\\banco-de-dados\\ERP00020002" + "." + (mes);
				}
				else {
					filePath = "/Users/albertsamuelmelo/TesteContabil/"+ (lineBreakCounter.getFilename());
				}
				
				System.out.println(filePath);
				
				ApplicationUI ui = (ApplicationUI) UI.getCurrent();

				
				
				try {
					ui.database.openConnection();
					Table tblTabelaConversao = ui.database.loadTableByName("tabelaconversao");
					
					BufferedWriter buffWrite = new BufferedWriter(new FileWriter(filePath));
					
					int contador = 0;
					
					int cLineCounter = 0;
					
					// É agora a hora do trabalho bracal
					for (String linha : lineBreakCounter.arquivo) {
						if ((contador>0) && (linha.length()>=200)) {
							String novaLinha = "";

							String lote = linha.substring(0, 20);
							String historico = linha.substring(20, 60);
							String conta = linha.substring(60,74);
							String valor = linha.substring(182, 212);
							String data = linha.substring(160, 170);
							String compl_Historico = linha.substring(80, 120);
							String usuario = linha.substring(120, 129);
							
							valor = valor.replace(",", "").trim();
							compl_Historico = historico + " " + compl_Historico;
							
							String tipoLanc = linha.substring(180, 181);

							String novaConta =  "0000000000" ;
							
							String novoValor = valor;
							
							if (data.contains("JAN")) {
								data = data.substring(0, 2) + "/" + "01" + "/" + data.substring(7, 9);
							}else if (data.contains("FEV")) {
								data = data.substring(0, 2) + "/" + "02" + "/" + data.substring(7, 9);
							}else if (data.contains("MAR")) {
								data = data.substring(0, 2) + "/" + "03" + "/" + data.substring(7, 9);
							}else if (data.contains("ABR")) {
								data = data.substring(0, 2) + "/" + "04" + "/" + data.substring(7, 9);
							}else if (data.contains("MAI")) {
								data = data.substring(0, 2) + "/" + "05" + "/" + data.substring(7, 9);
							}else if (data.contains("JUN")) {
								data = data.substring(0, 2) + "/" + "06" + "/" + data.substring(7, 9);
							}else if (data.contains("JUL")) {
								data = data.substring(0, 2) + "/" + "07" + "/" + data.substring(7, 9);
							}else if (data.contains("AGO")) {
								data = data.substring(0, 2) + "/" + "08" + "/" + data.substring(7, 9);
							}else if (data.contains("SET")) {
								data = data.substring(0, 2) + "/" + "09" + "/" + data.substring(7, 9);
							}else if (data.contains("OUT")) {
								data = data.substring(0, 2) + "/" + "10" + "/" + data.substring(7, 9);
							}else if (data.contains("NOV")) {
								data = data.substring(0, 2) + "/" + "11" + "/" + data.substring(7, 9);
							}else if (data.contains("DEZ")) {
								data = data.substring(0, 2) + "/" + "12" + "/" + data.substring(7, 9);
							}
							
							
							tblTabelaConversao.select("*");
							tblTabelaConversao.setFilter("codicont" , conta);
							tblTabelaConversao.loadData();
							if (!tblTabelaConversao.eof()) {
								novaConta = tblTabelaConversao.getString("codiresu");
							}
							else {
								
								
								tblTabelaConversao.insert();
								tblTabelaConversao.setValue("codicont", conta);
								tblTabelaConversao.setValue("codiresu", "0000000000");
								tblTabelaConversao.setValue("descricao", "Conta inexistente");
								tblTabelaConversao.execute();

								
								
								
								RmFormWindow formWindow = new RmFormWindow();
								formWindow.setTitle("Atenção!");
								formWindow.setWidth("600px");
								formWindow.setHeight("260px");
								formWindow.addMessage("Conta não cadastrada na tabela de conversao [" + conta + "]", "Na linha do arquivo" + (contador - 1) + ", Processamento CANCELADO!", MessageWindowType.WARNING);
								
								RmFormButtonBase tbmOk = formWindow.addButton("Ok");
								tbmOk.addRmFormButtonClickEventListener(new RmFormButtonClickEventListener() {
									@Override
									public void onRmFormButtonClick(RmFormButtonClickEvent event) {
										event.getWindow().close();
									}
								});
								
								formWindow.show();
							
								
								break;
							}
							
							/*
							String oldConta = conta;
							String novaConta = "";
							novaConta = oldConta.substring(0,1) + ".";
							novaConta += oldConta.substring(1, 2) + ".";
							novaConta += oldConta.substring(2, 3) + ".";
							novaConta += oldConta.substring(3, 5) + ".";
							novaConta += oldConta.substring(5, 6) + ".";
							novaConta += oldConta.substring(6, 7) + ".";
							novaConta += oldConta.substring(7, 9) + ".";
							novaConta += oldConta.substring(9, 14);
							
							System.out.println(novaConta);
							*/
														
							//Primeiro campo
								novaLinha += Utils.complete(" ", 5);
							//Fim primeiro campo
							
							//Segundo campo
							if (tipoLanc.equalsIgnoreCase("D")) {
								novaLinha += Utils.complete(novaConta, 18);	
								allMoneyFound = allMoneyFound + Double.parseDouble(novoValor);
							}
							else {
								novaLinha += Utils.complete("", 18);
							}
							//Fim segundo campo
							
							//Terceiro campo
							if (tipoLanc.equalsIgnoreCase("C")) {
								novaLinha += Utils.complete(novaConta, 18);	
								allMoneyFound = allMoneyFound - Double.parseDouble(novoValor);
							}
							else {
								novaLinha += Utils.complete(" ", 18);
							}
							//Fim terceiro campo
							
							//Quarto campo - Coluna produto
								novaLinha += Utils.complete(" ", 4);
							//Fim Quarto campo
							
							//Quinto campo
								novaLinha += Utils.complete(" ", 1);
							//Fim Quinto campo
							
							//Sexto campo
								if (novoValor.length() < 12) {
									int numDeZ = 12 - novoValor.length();
									String finalValue = "";
									for (int i = 0; i < numDeZ; i++) {
										finalValue += Utils.complete("0", 1);
									} 
									finalValue += Utils.complete(novoValor, 12-numDeZ);
									System.out.println(finalValue);
									novoValor = finalValue;
								}
								novaLinha += Utils.complete(novoValor, 12);
							//Fim sexto campo
										
							//Setimo campo
								novaLinha += Utils.complete(data, 10);
							//Fim setimo campo
								
							//Oitavo campo
								novaLinha += Utils.complete(" ", 6);
							//Fim oitavo campo
										
							//Nono campo
								novaLinha += Utils.complete(historico, 143);
							//Fim nono campo
								
							//Decimo campo
								novaLinha += Utils.complete(usuario, 20);
							//Fim decimo campo
								
							//11o campo
								novaLinha += Utils.complete("8", 20);
							//Fim 11o campo
								
							//12o campo
								novaLinha += Utils.complete(" ", 20);
							//Fim 12o campo
								
							//13o campo
								novaLinha += Utils.complete(" ", 15);
							//Fim 13o campo								
							
							//14o campo
								novaLinha += Utils.complete(" ", 20);
							//Fim 14o campo
								
							//15o campo
								novaLinha += Utils.complete(" ", 15);
							//Fim 15o campo
							
							//16o campo
								novaLinha += Utils.complete("M", 1);
							//Fim 16o campo
							
							//17o campo
								novaLinha += Utils.complete(" ", 4);								
							//Fim 17o campo
								
							//18o campo
								novaLinha += Utils.complete(" ", 10);								
							//Fim 18o campo
								
							novaLinha += "\r\n";
							
							arrayLinha.add(novaLinha);
							//buffWrite.append(novaLinha);
							
						}
						
						contador++;
					}
					
					lineCounter = contador;
					int j;
					String value;
					String compValue;
					
					for (int i = 0; i<lineCounter; i++) {
						
						value = arrayLinha.get(i);
						
						if(value == ""){
							continue;
						}else if( value.substring(5,17) != " ") {
							
							for (j = 0; j<lineCounter;j++) {
								if (j==i){
									continue;
								}
								compValue = arrayLinha.get(j);
								
								if(compValue == ""){
									continue;
								}else if( compValue.substring(23,40) != " ") {
								
									if (compValue.substring(46,58).equals(value.substring(46,58))) {
										
										System.out.println(value);
										System.out.println(compValue);
										
										buffWrite.append(value);
										buffWrite.append(compValue);
										
										
										
										arrayLinha.set(j, "");
										arrayLinha.set(i, "");
										//arrayLinha.remove(j);
										//lineCounter--;
										break;
									} else {
										continue;									
									}
								}else{
									continue;								
								}
							}	
						}
						System.out.println((i*100)/lineCounter + "%");
					}
					
					
					buffWrite.close();	

			        
				}
				catch (Exception e) {
					
				}
				finally {
					ui.database.closeConnection();
				}
			
				System.out.println(arrayLinha.size());

				
				
				System.out.println("All Money here: +" + allMoneyFound);
			}
		});
        
        layout.addComponent(sample);
	}
}
