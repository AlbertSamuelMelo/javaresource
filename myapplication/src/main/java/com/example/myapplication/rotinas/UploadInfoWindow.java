package com.example.myapplication.rotinas;

import com.vaadin.annotations.StyleSheet;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.ProgressBar;
import com.vaadin.ui.UI;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.FailedEvent;
import com.vaadin.ui.Upload.FinishedEvent;
import com.vaadin.ui.Upload.StartedEvent;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Window;


public class UploadInfoWindow extends Window implements Upload.StartedListener, Upload.ProgressListener, Upload.FailedListener, Upload.SucceededListener, Upload.FinishedListener {
    private final Label state = new Label();
    private final Label result = new Label();
    private final Label fileName = new Label();
    private final Label textualProgress = new Label();

    private final ProgressBar progressBar = new ProgressBar();
    private final Button cancelButton;
    private final LineBreakCounter counter;
    Upload upload = null;

    public UploadInfoWindow(final Upload upload, final LineBreakCounter lineBreakCounter) {
        super("Status");
        
        this.upload = upload;
        
        this.counter = lineBreakCounter;
        
        addStyleName("upload-info");

        setResizable(false);
        setDraggable(false);

        final FormLayout uploadInfoLayout = new FormLayout();
        setContent(uploadInfoLayout);
        uploadInfoLayout.setMargin(true);

        final HorizontalLayout stateLayout = new HorizontalLayout();
        stateLayout.setSpacing(true);
        stateLayout.addComponent(state);

        cancelButton = new Button("Cancel");
        cancelButton.addClickListener(event -> this.upload.interruptUpload());
        cancelButton.setVisible(false);
        cancelButton.setStyleName("small");
        stateLayout.addComponent(cancelButton);

        stateLayout.setCaption("Current state");
        state.setValue("Idle");
        uploadInfoLayout.addComponent(stateLayout);

        fileName.setCaption("File name");
        uploadInfoLayout.addComponent(fileName);

        result.setCaption("Line breaks counted");
        uploadInfoLayout.addComponent(result);

        progressBar.setCaption("Progress");
        progressBar.setVisible(false);
        uploadInfoLayout.addComponent(progressBar);

        textualProgress.setVisible(false);
        uploadInfoLayout.addComponent(textualProgress);

        this.upload.addStartedListener(this);
        this.upload.addProgressListener(this);
        this.upload.addFailedListener(this);
        this.upload.addSucceededListener(this);
        this.upload.addFinishedListener(this);

    }

    @Override
    public void uploadFinished(final FinishedEvent event) {
        state.setValue("Idle");
        progressBar.setVisible(false);
        textualProgress.setVisible(false);
        cancelButton.setVisible(false);
    }

    @Override
    public void uploadStarted(final StartedEvent event) {
        // this method gets called immediately after upload is started
        progressBar.setValue(0f);
        progressBar.setVisible(true);
        UI.getCurrent().setPollInterval(500);
        textualProgress.setVisible(true);
        // updates to client
        state.setValue("Uploading");
        fileName.setValue(event.getFilename());

        cancelButton.setVisible(true);
    }

    @Override
    public void updateProgress(final long readBytes, final long contentLength) {
        // this method gets called several times during the update
        progressBar.setValue(readBytes / (float) contentLength);
        textualProgress.setValue("Processed " + readBytes + " bytes of " + contentLength);
        result.setValue(counter.getLineBreakCount() + " (counting...)");
    }

    @Override
    public void uploadSucceeded(final SucceededEvent event) {
        result.setValue(counter.getLineBreakCount() + " (total)");
    }

    @Override
    public void uploadFailed(final FailedEvent event) {
        result.setValue(counter.getLineBreakCount() + " (counting interrupted at " + Math.round(100 * progressBar.getValue()) + "%)");
    }
}