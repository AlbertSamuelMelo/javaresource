package com.example.myapplication.rotinas;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.SucceededEvent;


public class LineBreakCounter  implements Receiver {
    private int counter;
    private int total;
    private boolean sleep;
    List<String> arquivo = new ArrayList<String>();
    String linha = ""; 
    private String name = "";

    /**
     * return an OutputStream that simply counts lineends
     */
    @Override
    public OutputStream receiveUpload(final String filename, final String MIMEType) {
        counter = 0;
        total = 0;
        name = filename;
        
        return new OutputStream() {
            private static final int searchedByte = '\n';
            private static final int retorno = '\r';

            @Override
            public void write(final int b) {
                total++;
            
                if (b!=retorno) {
                    if (b == searchedByte) {
                    	arquivo.add(linha);
                    	linha="";
                        counter++;
                    }
                    else {
                    	linha += Character.toString ((char) b);
                    }
                    
                    if (sleep && total % 1000 == 0) {
                        try {
                            Thread.sleep(100);
                        } catch (final InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        };
    }
    
    public String getFilename(){
    	return name;
    }
    
    public int getLineBreakCount() {
        return counter;
    }

    public void setSlow(boolean value) {
        sleep = value;
    }
}