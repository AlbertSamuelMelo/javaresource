package com.example.myapplication.rotinas;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.nio.file.Paths;
import java.util.ArrayList;

import com.evolucao.rmlibrary.database.Table;
import com.evolucao.rmlibrary.utils.Utils;
import com.evolucao.weblibrary.ApplicationUI;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.FinishedEvent;
import com.vaadin.ui.Upload.FinishedListener;
import com.vaadin.ui.VerticalLayout;

public class ImportaTabelaProcess extends VerticalLayout {
	
	public ImportaTabelaProcess()  {
		LineBreakCounter lineBreakCounter = new LineBreakCounter();
    	
        final VerticalLayout layout = new VerticalLayout();
        addComponent(layout);
        
        Upload sample = new Upload(null, lineBreakCounter);
        sample.setImmediateMode(false);
        sample.setButtonCaption("Upload File");
        sample.setSizeFull();
        
        UploadInfoWindow uploadInfoWindow = new UploadInfoWindow(sample, lineBreakCounter);
        
        sample.addStartedListener(event -> {
        	lineBreakCounter.arquivo = new ArrayList<String>();
        	lineBreakCounter.linha = "";
            if (uploadInfoWindow.getParent() == null) {
                UI.getCurrent().addWindow(uploadInfoWindow);
            }
            uploadInfoWindow.setClosable(false);
        });
        
        //sample.addFinishedListener(event -> uploadInfoWindow.setClosable(true));
        
        sample.addFinishedListener(new FinishedListener() {
			public void uploadFinished(FinishedEvent event) {
				lineBreakCounter.arquivo.add(lineBreakCounter.linha);
				uploadInfoWindow.setClosable(true);
				
				String filePath = "";
				
				String os = System.getProperty("os.name");
				
				System.out.println(os);
				
				if (os.contains("Windows")) {
					//filePath = Paths.get(".").toAbsolutePath().normalize().toString() + "\\banco-de-dados\\";
					filePath = "c:\\temp\\banco-de-dados\\Banco.txt";
				}
				else {
					filePath = "/Users/albertsamuelmelo/TesteContabil/Banco.txt";
				}
				
				System.out.println(filePath);
				ApplicationUI ui = (ApplicationUI) UI.getCurrent();
				
				try {
					//ui.database.openConnection();
					Table tblTabelaConversao = ui.database.loadTableByName("tabelaconversao");
					
					ui.database.openConnection();
					
					BufferedWriter buffWrite = new BufferedWriter(new FileWriter(filePath));

					int contador = 0;
					// É agora a hora do trabalho bracal
					for (String linha : lineBreakCounter.arquivo) {
						if (contador>0) {
							String contaCont = linha.substring(0, 21);
							String contaRed = linha.substring(44,54);
							System.out.println(contaCont + " - - - " + contaRed);
							
							while (contaCont.contains(".")) {
								contaCont = contaCont.replace(".", "");
							}
							
							tblTabelaConversao.select("*");
							tblTabelaConversao.setFilter("codicont", contaCont);
							tblTabelaConversao.loadData();
							if (tblTabelaConversao.eof()) {
								tblTabelaConversao.insert();
								tblTabelaConversao.setValue("codicont", contaCont);
								tblTabelaConversao.setValue("codiresu", contaRed);
								tblTabelaConversao.execute();
							}
						}
						contador++;
					}
					
					buffWrite.close();					
				}
				catch (Exception e) {
					
				}
				finally {
					ui.database.closeConnection();
				}
				
				System.out.println("Terminei!");
			}
		});
        
        layout.addComponent(sample);
	}
}
