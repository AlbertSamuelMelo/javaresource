package com.example.myapplication; 

import java.util.Date;
import java.util.UUID;

import com.evolucao.rmlibrary.database.Database;
import com.evolucao.rmlibrary.database.Table;
import com.evolucao.rmlibrary.database.TableRegistry;
import com.evolucao.weblibrary.ApplicationUI;
import com.evolucao.weblibrary.view.SystemView;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.UI;

public class Atualizacao {
	public static SystemView systemView = null;
	
	public Atualizacao() {
		
	}

	public static void execute() {
		ApplicationUI ui = (ApplicationUI) UI.getCurrent();
		//Database database = (Database) VaadinSession.getCurrent().getAttribute("database");
		
		try {
			ui.database.openConnection();
			
			for (TableRegistry tableRegistry: ui.database.getTableRegistryList()) {
				Table tblTabela = ui.database.loadTableByName(tableRegistry.getTableName());
				tblTabela.checkTable();
			}
			
			/*
			Table tblParametros = ui.database.loadTableByName("parametros");
			tblParametros.alterTable();
			
			Table tblPermissoes = ui.database.loadTableByName("permissoes");
			tblPermissoes.alterTable();
			
			Table tblDevoReci = ui.database.loadTableByName("devoreci");
			tblDevoReci.alterTable();
			
			Table tblSaidCaix = ui.database.loadTableByName("saidcaix");
			tblSaidCaix.alterTable();
			
			Table tblCarrComp2 = ui.database.loadTableByName("carrcomp2");
			tblCarrComp2.createTable();
			//tblCarrComp2.alterTable();
			
			Table tblCarrConsExam = ui.database.loadTableByName("carrconsexam");
			tblCarrConsExam.createTable();
			
			Table tblFornecedores = ui.database.loadTableByName("fornecedores");
			tblFornecedores.alterTable();
			
			Table tblMarcCons = ui.database.loadTableByName("marccons");
			tblMarcCons.alterTable();
			
			Table tblAssociados = ui.database.loadTableByName("associados");
			tblAssociados.alterTable();
			
			*/
			
			//Table tblUsuarios2 = ui.database.loadTableByName("associados2");
			//tblUsuarios2.createTable();

			updateUidSaidCaix();
			updateUidDevoReci();
			updateUidEspeForn();
			updateUidRecibos();
			updateUidMarcCons2();
			updateUidMedicos();
			updateUidEspecialidades();
			updateUidAssociados();
			updateUidPermissoes();

			
			// Retira da descricao da especialidade os codigos
			//updateDescricaoEspecialidades();
			
			// Retira do nome dos medicos os codigos
			//updateNomeMedicos();			
			
			/*
			Table tblUsuarios = ui.database.loadTableByName("usuarios");
			tblUsuarios.alterTable();
			
			Table tblEspeMedi = ui.database.loadTableByName("espemedi");
			tblEspeMedi.alterTable();
			
			Table tblPermissoes = ui.database.loadTableByName("permissoes");
			tblPermissoes.alterTable();
			
			Table tblGrupUsua = ui.database.loadTableByName("grupusua");
			tblGrupUsua.alterTable();
			
			Table tblCarrCons = ui.database.loadTableByName("carrcons");
			tblCarrCons.alterTable();
			
			Table tblCarrExamLabo = ui.database.loadTableByName("carrexamlabo");
			tblCarrExamLabo.alterTable();
			*/
			
			//Table tblParametros = ui.database.loadTableByName("parametros");
			//tblParametros.alterTable();
			
			//Table tblMenu = ui.database.loadTableByName("menu");
			//tblMenu.alterTable();
			
			//Table tblAssociados = ui.database.loadTableByName("associados");
			//tblAssociados.alterTable();
			
			//ui.database.executeCommand("update menu set projectName='prosaude' where menu.projectName is null");
			
			//ui.database.executeCommand("update grupusua set projectName='prosaude' where projectName is null");
			
			//ui.database.executeCommand("update usuarios set projectName='prosaude' where projectName is null");
			
			//ui.database.executeCommand("update permissoes set projectName='prosaude' where projectName is null");
			
			//updateUidPermissoes();
			
			//createMenu();
			
			//Table tblMarcCons = ui.database.loadTableByName("marccons");
			//tblMarcCons.alterTable();
			
			//updateUidEspecialidades();
			//updateUidMarcCons();			

			/*
			Table tblSequCaixC32 = ui.database.loadTableByName("sequcaixc3");
			Table tblSequCaixC3 = ui.database.loadTableByName("sequcaixc3");
			tblSequCaixC3.alterTable();
			
			while (true) {
				tblSequCaixC3.select("*");
				tblSequCaixC3.setWhere("(sequcaixc3.uid is null)");
				tblSequCaixC3.setLimit(100);
				tblSequCaixC3.loadData();
				if (tblSequCaixC3.eof()) {
					break;
				}
					
				while (!tblSequCaixC3.eof()) {
					tblSequCaixC32.update();
					tblSequCaixC32.setValue("uid", UUID.randomUUID().toString().toUpperCase());
					tblSequCaixC32.setFilter("sequencia", tblSequCaixC3.getInteger("sequencia"));
					tblSequCaixC32.execute();
					
					tblSequCaixC3.next();
				}
			}
			System.out.println("Processo concluido!");
			*/

			/*
			Table tblSequCaixC22 = ui.database.loadTableByName("sequcaixc2");
			Table tblSequCaixC2 = ui.database.loadTableByName("sequcaixc2");
			tblSequCaixC2.alterTable();
			
			while (true) {
				tblSequCaixC2.select("*");
				tblSequCaixC2.setWhere("(sequcaixc2.uid is null)");
				tblSequCaixC2.setLimit(100);
				tblSequCaixC2.loadData();
				if (tblSequCaixC2.eof()) {
					break;
				}
					
				while (!tblSequCaixC2.eof()) {
					tblSequCaixC22.update();
					tblSequCaixC22.setValue("uid", UUID.randomUUID().toString().toUpperCase());
					tblSequCaixC22.setFilter("sequencia", tblSequCaixC2.getInteger("sequencia"));
					tblSequCaixC22.execute();
					
					tblSequCaixC2.next();
				}
			}
			System.out.println("Processo concluido!");
			*/
			
			/*
			Table tblSequCaixc12 = ui.database.loadTableByName("sequcaixc1");
			Table tblSequCaixc1 = ui.database.loadTableByName("sequcaixc1");
			tblSequCaixc1.alterTable();
			
			while (true) {
				tblSequCaixc1.select("*");
				tblSequCaixc1.setWhere("(sequcaixc1.uid is null)");
				tblSequCaixc1.setLimit(100);
				tblSequCaixc1.loadData();
				if (tblSequCaixc1.eof()) {
					break;
				}
					
				while (!tblSequCaixc1.eof()) {
					tblSequCaixc12.update();
					tblSequCaixc12.setValue("uid", UUID.randomUUID().toString().toUpperCase());
					tblSequCaixc12.setFilter("sequencia", tblSequCaixc1.getInteger("sequencia"));
					tblSequCaixc12.execute();
					
					tblSequCaixc1.next();
				}
			}
			System.out.println("Processo concluido!");
			*/
			
			/*
			Table tblCaixUsua = ui.database.loadTableByName("caixusua");
			tblCaixUsua.createTable();
			
			Table tblRecibos2 = ui.database.loadTableByName("recibos");
			Table tblRecibos = ui.database.loadTableByName("recibos");
			tblRecibos.alterTable();
			
			while (true) {
				tblRecibos.select("*");
				tblRecibos.setWhere("(recibos.uid is null)");
				tblRecibos.setLimit(100);
				tblRecibos.loadData();
				if (tblRecibos.eof()) {
					break;
				}
				while (!tblRecibos.eof()) {
					tblRecibos2.update();
					tblRecibos2.setValue("uid", UUID.randomUUID().toString().toUpperCase());
					tblRecibos2.setFilter("sequencia", tblRecibos.getInteger("sequencia"));
					tblRecibos2.execute();
					
					tblRecibos.next();
				}
			}
			System.out.println("Processo Concluido!");
			
			Table tblCaixas = ui.database.loadTableByName("caixas");
			tblCaixas.alterTable();
			
			Table tblCaixas2 = ui.database.loadTableByName("caixas");
			
			tblCaixas.select("*");
			tblCaixas.loadData();
			while (!tblCaixas.eof()) {
				tblCaixas2.update();
				tblCaixas2.setValue("uid", UUID.randomUUID().toString().toUpperCase());
				tblCaixas2.setFilter("codicaix", tblCaixas.getString("codicaix"));
				tblCaixas2.execute();
				
				tblCaixas.next();
			}
			System.out.println("Processo concluido!");
			
			Table tblEspecialidades = ui.database.loadTableByName("especialidades");
			tblEspecialidades.alterTable();
			
			Table tblFornecedores = ui.database.loadTableByName("fornecedores");
			tblFornecedores.alterTable();
			*/
			
			/*
			Table tblCarrComp = ui.database.loadTableByName("carrcomp");
			tblCarrComp.createTable();
			//tblCarrComp.alterTable();

			Table tblCarrCons = ui.database.loadTableByName("carrcons");
			tblCarrCons.createTable();
			//tblCarrCons.alterTable();

			Table tblCarrExamLabo = ui.database.loadTableByName("carrexamlabo");
			tblCarrExamLabo.createTable();
			//tblCarrExamLabo.alterTable();
			 */
			
			/*
			Table tblAssociados = ui.database.loadTableByName("associados");
			tblAssociados.alterTable();
			*/
			
			
			/*
			//Table tblAssociados = ui.database.loadTableByName("associados");
			Table tblAssociados2 = ui.database.loadTableByName("associados");
			
			while (true) {
				tblAssociados.select("*");
				tblAssociados.setWhere("uid is null");
				tblAssociados.setLimit(100);
				tblAssociados.loadData();
				if (tblAssociados.eof()) {
					break;
				}
				
				while (!tblAssociados.eof()) {
					tblAssociados2.update();
					tblAssociados2.setValue("uid", UUID.randomUUID().toString().toUpperCase());
					tblAssociados2.setFilter("sequencia", tblAssociados.getInteger("sequencia"));
					tblAssociados2.execute();
					tblAssociados.next();
				}
			}
			System.out.println("Processo concluido!");
			*/
			
			//Table tblEspeForn = Database.Instance.loadTableByName("espeforn");
			//tblEspeForn.createTable();
			//tblEspeForn.alterTable();

			/*
			System.out.println("Atualizando usuarios...");
			Table tblUsuarios = Database.Instance.loadTableByName("usuarios");
			Table tblUsuarios2 = Database.Instance.loadTableByName("usuarios");
			tblUsuarios.alterTable();
			
			while (true) {
				tblUsuarios.select("*");
    			tblUsuarios.setWhere("uid is null");
				tblUsuarios.setLimit(100);
				tblUsuarios.loadData();
				if (tblUsuarios.eof()) {
					break;
				}
				while (!tblUsuarios.eof()) {
					tblUsuarios2.update();
					tblUsuarios2.setValue("uid", UUID.randomUUID().toString().toUpperCase());
					tblUsuarios2.setFilter("login", tblUsuarios.getString("login"));
					tblUsuarios2.execute();
					
					System.out.println("processando " + tblUsuarios.getString("login"));
					tblUsuarios.next();
				}
			}
			*/
			
			/*
			 * Manutencao em grupo de usuarios
			 *
			 * */
			/*
			System.out.println("Atualizando grupo de usuarios...");
			Table tblGrupUsua = Database.Instance.loadTableByName("grupusua");
			tblGrupUsua.alterTable();
			tblGrupUsua.select("*");
			tblGrupUsua.loadData();
			Table tblGrupUsua2 = Database.Instance.loadTableByName("grupusua"); 
			
			while (!tblGrupUsua.eof()) {
				tblGrupUsua2.update();
				tblGrupUsua2.setValue("uid", UUID.randomUUID().toString().toUpperCase());
				tblGrupUsua2.setFilter("idgrupo", tblGrupUsua.getInteger("idgrupo"));
				tblGrupUsua2.execute();
				
				tblGrupUsua.next();
			}
			*/
			/*
			System.out.println("Atualizando menu...");
			Table tblMenu = Database.Instance.loadTableByName("menu");
			Table tblMenu2 = Database.Instance.loadTableByName("menu");
			tblMenu.alterTable();
			tblMenu.select("*");
			tblMenu.loadData();
			while (!tblMenu.eof()) {
				tblMenu2.update();
				tblMenu2.setValue("uid", UUID.randomUUID().toString().toUpperCase());
				tblMenu2.setFilter("idmenu", tblMenu.getInteger("idmenu"));
				tblMenu2.execute();
				tblMenu.next();
			}
			*/
			
			/*
			*/
			
			/*
			System.out.println("Atualizando fornecedores...");
			Table tblFornecedores = Database.Instance.loadTableByName("fornecedores");
			Table tblFornecedores2 = Database.Instance.loadTableByName("fornecedores");
			tblFornecedores.alterTable();
			tblFornecedores.select("*");
			tblFornecedores.loadData();
			while (!tblFornecedores.eof()) {
				tblFornecedores2.update();
				tblFornecedores2.setValue("uid", UUID.randomUUID().toString().toUpperCase());
				tblFornecedores2.setFilter("codiforn", tblFornecedores.getInteger("codiforn"));
				tblFornecedores2.execute();
				tblFornecedores.next();
			}
			System.out.println("Processo concluido!");
			*/

			//ApplicationUI ui = (ApplicationUI) UI.getCurrent();
			

			/*
			Table tblEspeMedi = Database.Instance.loadTableByName("espemedi");
			Table tblEspeMedi2 = Database.Instance.loadTableByName("espemedi");
			Table tblMedicos = Database.Instance.loadTableByName("medicos");

			tblMedicos = Database.Instance.loadTableByName("medicos");
			//Table tblMedicos2 = Database.Instance.loadTableByName("medicos");
			//tblMedicos.alterTable();
			
			/*
			System.out.println("Atualizando medicos...");
			tblMedicos.select("*");
			tblMedicos.loadData();
			while (!tblMedicos.eof()) {
				tblMedicos2.update();
				tblMedicos2.setValue("uid", UUID.randomUUID().toString().toUpperCase());
				tblMedicos2.setFilter("codimedi", tblMedicos.getInteger("codimedi"));
				tblMedicos2.execute();
				
				tblMedicos.next();
			}
			System.out.println("processo concluido!");
			*/
			
			/*
			Table tblAgenda = Database.Instance.loadTableByName("agenda");
			tblAgenda.createTable();
			
			System.out.println("Atualizando espemedi...");
			tblEspeMedi.alterTable();
			tblEspeMedi.select("*");
			tblEspeMedi.loadData();
			while (!tblEspeMedi.eof()) {
				tblMedicos.select("*");
				tblMedicos.setFilter("codimedi", tblEspeMedi.getInteger("codimedi"));
				tblMedicos.loadData();
				
				tblEspeMedi2.update();
				tblEspeMedi2.setValue("uid", UUID.randomUUID().toString().toUpperCase());
				tblEspeMedi2.setValue("uidmedico", tblMedicos.getString("uid"));
				tblEspeMedi2.setFilter("sequencia", tblEspeMedi.getInteger("sequencia"));
				tblEspeMedi2.execute();
				
				tblEspeMedi.next();
			}
			System.out.println("processo concluido!");

			/*
			System.out.println("Atualizando prefixo...");
			Table tblPrefixo = Database.Instance.loadTableByName("prefixo");
			Table tblPrefixo2 = Database.Instance.loadTableByName("prefixo");
			tblPrefixo.alterTable();
			tblPrefixo.select("*");
			tblPrefixo.loadData();
			while (!tblPrefixo.eof()) {
				tblPrefixo2.update();
				tblPrefixo2.setValue("uid", UUID.randomUUID().toString().toUpperCase());
				tblPrefixo2.setFilter("prefixo", tblPrefixo.getString("prefixo"));
				tblPrefixo2.execute();
				tblPrefixo.next();
			}
			System.out.println("Processo concluido!");
			
			System.out.println("Atualizando cep...");
			Table tblCep = Database.Instance.loadTableByName("cep");
			Table tblCep2 = Database.Instance.loadTableByName("cep");
			tblCep.alterTable();

			while (true) {
				tblCep.select("*");
				tblCep.setLimit(100);
				tblCep.setWhere("uid is null");
				tblCep.loadData();
				if (tblCep.eof()) {
					break;
				}
				else {
					while (!tblCep.eof()) {
						//System.out.println("Processando: " + tblCep.getString("cep"));
						tblCep2.update();
						tblCep2.setValue("uid", UUID.randomUUID().toString().toUpperCase());
						tblCep2.setFilter("cep", tblCep.getString("cep"));
						tblCep2.execute();
						tblCep.next();
					}
				}
			}
			System.out.println("processo concluido!");
			*/
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
		finally {
			ui.database.closeConnection();
		}
	}
	
	public void processaControle() {
		ApplicationUI ui = (ApplicationUI) UI.getCurrent();

		Table tblControle1 = ui.database.loadTableByName("controle1");
		tblControle1.createTable();
		
		Table tblControle2 = ui.database.loadTableByName("controle2");
		tblControle2.createTable();

		Table tblControle3 = ui.database.loadTableByName("controle3");
		tblControle3.createTable();
		
		Table tblRecibos = ui.database.loadTableByName("recibos");
		Table tblRecibos2 = ui.database.loadTableByName("recibos");
		tblRecibos.alterTable();
		
		Table tblMarcCons = ui.database.loadTableByName("marccons");

		boolean continuar = true;
		while (continuar) {
			tblRecibos.select("*");
			tblRecibos.setWhere("(emissao>='2017-01-01 00:00:00') and (emissao<='2017-10-18 23:59:59') and (processado is null)");
			//tblRecibos.setOrder("emissao");
			tblRecibos.setOrder("incldata");
			tblRecibos.setLimit(1000);
			tblRecibos.loadData();
			if (tblRecibos.eof()) {
				continuar=false;
			}
			else {
				while (!tblRecibos.eof()) {
					System.out.println("Processando recigo: " + tblRecibos.getInteger("sequencia") + " de " + tblRecibos.getDate("emissao"));
					
					tblMarcCons.select("*");
					tblMarcCons.setFilter("sequencia", tblRecibos.getInteger("sequproc"));
					tblMarcCons.loadData();
					
					if (tblMarcCons.eof()) {
						System.out.println("NAO ENCONTROU GUIA DO RECIBO " + tblRecibos.getInteger("sequencia") + " - " + tblRecibos.getString("referente"));
						System.out.println("----------------------------------------------------");
						
						// Atualiza o recibo que ja foi processado
						tblRecibos2.setAuditing(false);
						tblRecibos2.update();
						tblRecibos2.setValue("processado", "nem");
						tblRecibos2.setFilter("sequencia", tblRecibos.getString("sequencia"));
						tblRecibos2.execute();
					}
					else {
						if (tblMarcCons.getInteger("sequencia")==591746) {
							System.out.println("Chegou");
						}
						
						processaItem(tblRecibos.getInteger("codiforn"), tblMarcCons.getInteger("codiespe"), tblMarcCons.getDouble("codiespe_vlrcusto"), tblMarcCons.getDouble("codiespe_valor"), tblRecibos.getDate("emissao"), tblRecibos.getInteger("sequencia"));
						processaItem(tblRecibos.getInteger("codiforn"), tblMarcCons.getInteger("codiproc01"), tblMarcCons.getDouble("codiproc01_vlrcusto"), tblMarcCons.getDouble("codiproc01_valor"), tblRecibos.getDate("emissao"), tblRecibos.getInteger("sequencia"));
						processaItem(tblRecibos.getInteger("codiforn"), tblMarcCons.getInteger("codiproc02"), tblMarcCons.getDouble("codiproc02_vlrcusto"), tblMarcCons.getDouble("codiproc02_valor"), tblRecibos.getDate("emissao"), tblRecibos.getInteger("sequencia"));
						processaItem(tblRecibos.getInteger("codiforn"), tblMarcCons.getInteger("codiproc03"), tblMarcCons.getDouble("codiproc03_vlrcusto"), tblMarcCons.getDouble("codiproc03_valor"), tblRecibos.getDate("emissao"), tblRecibos.getInteger("sequencia"));
						processaItem(tblRecibos.getInteger("codiforn"), tblMarcCons.getInteger("codiproc04"), tblMarcCons.getDouble("codiproc04_vlrcusto"), tblMarcCons.getDouble("codiproc04_valor"), tblRecibos.getDate("emissao"), tblRecibos.getInteger("sequencia"));
						processaItem(tblRecibos.getInteger("codiforn"), tblMarcCons.getInteger("codiproc05"), tblMarcCons.getDouble("codiproc05_vlrcusto"), tblMarcCons.getDouble("codiproc05_valor"), tblRecibos.getDate("emissao"), tblRecibos.getInteger("sequencia"));
						processaItem(tblRecibos.getInteger("codiforn"), tblMarcCons.getInteger("codiproc06"), tblMarcCons.getDouble("codiproc06_vlrcusto"), tblMarcCons.getDouble("codiproc06_valor"), tblRecibos.getDate("emissao"), tblRecibos.getInteger("sequencia"));
						processaItem(tblRecibos.getInteger("codiforn"), tblMarcCons.getInteger("codiproc07"), tblMarcCons.getDouble("codiproc07_vlrcusto"), tblMarcCons.getDouble("codiproc07_valor"), tblRecibos.getDate("emissao"), tblRecibos.getInteger("sequencia"));
						processaItem(tblRecibos.getInteger("codiforn"), tblMarcCons.getInteger("codiproc08"), tblMarcCons.getDouble("codiproc08_vlrcusto"), tblMarcCons.getDouble("codiproc08_valor"), tblRecibos.getDate("emissao"), tblRecibos.getInteger("sequencia"));
						processaItem(tblRecibos.getInteger("codiforn"), tblMarcCons.getInteger("codiproc09"), tblMarcCons.getDouble("codiproc09_vlrcusto"), tblMarcCons.getDouble("codiproc09_valor"), tblRecibos.getDate("emissao"), tblRecibos.getInteger("sequencia"));
						processaItem(tblRecibos.getInteger("codiforn"), tblMarcCons.getInteger("codiproc10"), tblMarcCons.getDouble("codiproc10_vlrcusto"), tblMarcCons.getDouble("codiproc10_valor"), tblRecibos.getDate("emissao"), tblRecibos.getInteger("sequencia"));
						
						processaItem(tblRecibos.getInteger("codiforn"), tblMarcCons.getInteger("codiproc11"), tblMarcCons.getDouble("codiproc11_vlrcusto"), tblMarcCons.getDouble("codiproc11_valor"), tblRecibos.getDate("emissao"), tblRecibos.getInteger("sequencia"));
						processaItem(tblRecibos.getInteger("codiforn"), tblMarcCons.getInteger("codiproc12"), tblMarcCons.getDouble("codiproc12_vlrcusto"), tblMarcCons.getDouble("codiproc12_valor"), tblRecibos.getDate("emissao"), tblRecibos.getInteger("sequencia"));
						processaItem(tblRecibos.getInteger("codiforn"), tblMarcCons.getInteger("codiproc13"), tblMarcCons.getDouble("codiproc13_vlrcusto"), tblMarcCons.getDouble("codiproc13_valor"), tblRecibos.getDate("emissao"), tblRecibos.getInteger("sequencia"));
						processaItem(tblRecibos.getInteger("codiforn"), tblMarcCons.getInteger("codiproc14"), tblMarcCons.getDouble("codiproc14_vlrcusto"), tblMarcCons.getDouble("codiproc14_valor"), tblRecibos.getDate("emissao"), tblRecibos.getInteger("sequencia"));
						processaItem(tblRecibos.getInteger("codiforn"), tblMarcCons.getInteger("codiproc15"), tblMarcCons.getDouble("codiproc15_vlrcusto"), tblMarcCons.getDouble("codiproc15_valor"), tblRecibos.getDate("emissao"), tblRecibos.getInteger("sequencia"));
						processaItem(tblRecibos.getInteger("codiforn"), tblMarcCons.getInteger("codiproc16"), tblMarcCons.getDouble("codiproc16_vlrcusto"), tblMarcCons.getDouble("codiproc16_valor"), tblRecibos.getDate("emissao"), tblRecibos.getInteger("sequencia"));
						processaItem(tblRecibos.getInteger("codiforn"), tblMarcCons.getInteger("codiproc17"), tblMarcCons.getDouble("codiproc17_vlrcusto"), tblMarcCons.getDouble("codiproc17_valor"), tblRecibos.getDate("emissao"), tblRecibos.getInteger("sequencia"));
						processaItem(tblRecibos.getInteger("codiforn"), tblMarcCons.getInteger("codiproc18"), tblMarcCons.getDouble("codiproc18_vlrcusto"), tblMarcCons.getDouble("codiproc18_valor"), tblRecibos.getDate("emissao"), tblRecibos.getInteger("sequencia"));
						processaItem(tblRecibos.getInteger("codiforn"), tblMarcCons.getInteger("codiproc19"), tblMarcCons.getDouble("codiproc19_vlrcusto"), tblMarcCons.getDouble("codiproc19_valor"), tblRecibos.getDate("emissao"), tblRecibos.getInteger("sequencia"));
						processaItem(tblRecibos.getInteger("codiforn"), tblMarcCons.getInteger("codiproc20"), tblMarcCons.getDouble("codiproc20_vlrcusto"), tblMarcCons.getDouble("codiproc20_valor"), tblRecibos.getDate("emissao"), tblRecibos.getInteger("sequencia"));
						
						processaItem(tblRecibos.getInteger("codiforn"), tblMarcCons.getInteger("codiproc21"), tblMarcCons.getDouble("codiproc21_vlrcusto"), tblMarcCons.getDouble("codiproc21_valor"), tblRecibos.getDate("emissao"), tblRecibos.getInteger("sequencia"));
						processaItem(tblRecibos.getInteger("codiforn"), tblMarcCons.getInteger("codiproc22"), tblMarcCons.getDouble("codiproc22_vlrcusto"), tblMarcCons.getDouble("codiproc22_valor"), tblRecibos.getDate("emissao"), tblRecibos.getInteger("sequencia"));
						processaItem(tblRecibos.getInteger("codiforn"), tblMarcCons.getInteger("codiproc23"), tblMarcCons.getDouble("codiproc23_vlrcusto"), tblMarcCons.getDouble("codiproc23_valor"), tblRecibos.getDate("emissao"), tblRecibos.getInteger("sequencia"));
						processaItem(tblRecibos.getInteger("codiforn"), tblMarcCons.getInteger("codiproc24"), tblMarcCons.getDouble("codiproc24_vlrcusto"), tblMarcCons.getDouble("codiproc24_valor"), tblRecibos.getDate("emissao"), tblRecibos.getInteger("sequencia"));
						processaItem(tblRecibos.getInteger("codiforn"), tblMarcCons.getInteger("codiproc25"), tblMarcCons.getDouble("codiproc25_vlrcusto"), tblMarcCons.getDouble("codiproc25_valor"), tblRecibos.getDate("emissao"), tblRecibos.getInteger("sequencia"));
						processaItem(tblRecibos.getInteger("codiforn"), tblMarcCons.getInteger("codiproc26"), tblMarcCons.getDouble("codiproc26_vlrcusto"), tblMarcCons.getDouble("codiproc26_valor"), tblRecibos.getDate("emissao"), tblRecibos.getInteger("sequencia"));
						processaItem(tblRecibos.getInteger("codiforn"), tblMarcCons.getInteger("codiproc27"), tblMarcCons.getDouble("codiproc27_vlrcusto"), tblMarcCons.getDouble("codiproc27_valor"), tblRecibos.getDate("emissao"), tblRecibos.getInteger("sequencia"));
						processaItem(tblRecibos.getInteger("codiforn"), tblMarcCons.getInteger("codiproc28"), tblMarcCons.getDouble("codiproc28_vlrcusto"), tblMarcCons.getDouble("codiproc28_valor"), tblRecibos.getDate("emissao"), tblRecibos.getInteger("sequencia"));
						processaItem(tblRecibos.getInteger("codiforn"), tblMarcCons.getInteger("codiproc29"), tblMarcCons.getDouble("codiproc29_vlrcusto"), tblMarcCons.getDouble("codiproc29_valor"), tblRecibos.getDate("emissao"), tblRecibos.getInteger("sequencia"));
						processaItem(tblRecibos.getInteger("codiforn"), tblMarcCons.getInteger("codiproc30"), tblMarcCons.getDouble("codiproc30_vlrcusto"), tblMarcCons.getDouble("codiproc30_valor"), tblRecibos.getDate("emissao"), tblRecibos.getInteger("sequencia"));
						
						processaItem(tblRecibos.getInteger("codiforn"), tblMarcCons.getInteger("codiproc31"), tblMarcCons.getDouble("codiproc31_vlrcusto"), tblMarcCons.getDouble("codiproc31_valor"), tblRecibos.getDate("emissao"), tblRecibos.getInteger("sequencia"));
						processaItem(tblRecibos.getInteger("codiforn"), tblMarcCons.getInteger("codiproc32"), tblMarcCons.getDouble("codiproc32_vlrcusto"), tblMarcCons.getDouble("codiproc32_valor"), tblRecibos.getDate("emissao"), tblRecibos.getInteger("sequencia"));
						
						// Atualiza o recibo que ja foi processado
						tblRecibos2.setAuditing(false);
						tblRecibos2.update();
						tblRecibos2.setValue("processado", "ok");
						tblRecibos2.setFilter("sequencia", tblRecibos.getString("sequencia"));
						tblRecibos2.execute();
					}
					
					
					tblRecibos.next();
				}
			}
		}
	
		System.out.println("processo concluido!");
	}
	
	
	public static void processaItem(Integer codiforn, Integer codiespe, double vlrCusto, double vlrVenda, Date data, Integer numereci) {
		ApplicationUI ui = (ApplicationUI) UI.getCurrent();
		
		if ((codiespe!=null) && (codiespe!=0) && (vlrCusto!=0) && (vlrVenda!=0)) {
			Table tblControle1 = ui.database.loadTableByName("controle1");
			Table tblControle2 = ui.database.loadTableByName("controle2");
			Table tblControle3 = ui.database.loadTableByName("controle3");
			Table tblEspecialidades = ui.database.loadTableByName("especialidades");
			
			tblEspecialidades.select("*");
			tblEspecialidades.setFilter("codiespe", codiespe);
			tblEspecialidades.loadData();
			
			tblControle1.select("*");
			tblControle1.setFilter("codiforn", codiforn);
			tblControle1.setFilter("codiespe", codiespe);
			tblControle1.loadData();
			
			// Caso não tenha encontrado registro
			if (tblControle1.eof()) {
				// Inclui o cadastro do produto
				tblControle1.insert();
				tblControle1.setValue("codiforn", codiforn);
				tblControle1.setValue("codiespe", codiespe);
				tblControle1.setValue("descricao", tblEspecialidades.getString("descricao"));
				tblControle1.setValue("data", data); // Data em que foi registrado a primeira movimentacao do item
				tblControle1.setValue("vlrcusto", vlrCusto); // Valor custo da primeira movimentacao do item
				tblControle1.setValue("vlrvenda", vlrVenda); // Valor venda da primeira movimentacao do item
				tblControle1.setValue("lastModify", data); // Data da ultima vez que o valor de venda ou custo do item foi modificado
				tblControle1.setValue("lastVlrCusto", vlrCusto); // Valor de custo na ultima vez que o item foi modificado
				tblControle1.setValue("lastVlrVenda", vlrVenda); // Valor de venda na ultima vez que o item foi modificado
				tblControle1.setValue("numeroAlteracoes", 0);
				tblControle1.setValue("numeultireci", numereci);
				tblControle1.execute();
				
				// Inclui o primeiro registro de totalizacao
				tblControle2.insert();
				tblControle2.setValue("uidcontrole1", tblControle1.getUidLastRecordProccessed());
				tblControle2.setValue("codiforn", codiforn);
				tblControle2.setValue("codiespe", codiespe);
				tblControle2.setValue("data", data);
				tblControle2.setValue("alterado", "N");
				tblControle2.setValue("quantidade", 1);
				tblControle2.setValue("vlrcusto", vlrCusto);
				tblControle2.setValue("vlrvenda", vlrVenda);
				tblControle2.setValue("vlrcustototal", vlrCusto);
				tblControle2.setValue("vlrvendatotal", vlrVenda);
				tblControle2.setValue("numeultireci", numereci);
				tblControle2.execute();
				
				// Inclui o registro de movimentacao do item
				tblControle3.insert();
				tblControle3.setValue("uidcontrole1", tblControle1.getUidLastRecordProccessed());
				tblControle3.setValue("numereci", numereci);
				tblControle3.setValue("codiforn", codiforn);
				tblControle3.setValue("codiespe", codiespe);
				tblControle3.setValue("data", data);
				tblControle3.setValue("alterado", "N");
				tblControle3.setValue("vlrcusto", vlrCusto);
				tblControle3.setValue("vlrvenda", vlrVenda);
				tblControle3.execute();
			}
			else {
				// Caso o valor de custo ou valor de venda seja diferente da ultima movimentacao
				if ((vlrCusto!=tblControle1.getDouble("lastVlrCusto")) || (vlrVenda!=tblControle1.getDouble("lastVlrVenda"))) {
					String uid = tblControle1.getString("uid");
					Integer numeroAlteracoes = tblControle1.getInteger("numeroAlteracoes");
					
					tblControle1.update();
					tblControle1.setValue("lastModify", data); // Data da ultima vez que o valor de venda ou custo do item foi modificado
					tblControle1.setValue("lastVlrCusto", vlrCusto); // Valor de custo na ultima vez que o item foi modificado
					tblControle1.setValue("lastVlrVenda", vlrVenda); // Valor de venda na ultima vez que o item foi modificado
					tblControle1.setValue("numeroAlteracoes", numeroAlteracoes+1);
					tblControle1.setValue("numeultireci", numereci);
					tblControle1.setFilter("uid", uid);
					tblControle1.execute();
					
					tblControle2.insert();
					tblControle2.setValue("uidcontrole1", uid);
					tblControle2.setValue("codiforn", codiforn);
					tblControle2.setValue("codiespe", codiespe);
					tblControle2.setValue("data", data);
					tblControle2.setValue("alterado", "S");
					tblControle2.setValue("quantidade", 1);
					tblControle2.setValue("vlrcusto", vlrCusto);
					tblControle2.setValue("vlrvenda", vlrVenda);
					tblControle2.setValue("numeultireci", numereci);
					tblControle2.execute();
					
					tblControle3.insert();
					tblControle3.setValue("uidcontrole1", uid);
					tblControle3.setValue("numereci", numereci);
					tblControle3.setValue("codiforn", codiforn);
					tblControle3.setValue("codiespe", codiespe);
					tblControle3.setValue("data", data);
					tblControle3.setValue("alterado", "S");
					tblControle3.setValue("vlrcusto", vlrCusto);
					tblControle3.setValue("vlrvenda", vlrVenda);
					tblControle3.execute();
				}
				else {
					tblControle2.select("*");
					tblControle2.setFilter("codiforn", codiforn);
					tblControle2.setFilter("codiespe", codiespe);
					tblControle2.setOrder("data desc");
					tblControle2.setLimit(1);
					tblControle2.loadData();
					
					String uid = tblControle2.getString("uid");
					Double vlrcustototal = tblControle2.getDouble("vlrcustototal");
					Double vlrvendatotal = tblControle2.getDouble("vlrvendatotal");
					Integer quantidade = tblControle2.getInteger("quantidade");
					
					tblControle2.update();
					tblControle2.setValue("quantidade", quantidade+1);
					tblControle2.setValue("vlrcustototal", vlrcustototal+vlrCusto);
					tblControle2.setValue("vlrvendatotal", vlrvendatotal+vlrVenda);
					tblControle2.setValue("numeultireci", numereci);
					tblControle2.setFilter("uid", uid);
					tblControle2.execute();
					
					tblControle3.insert();
					tblControle3.setValue("uidcontrole1", uid);
					tblControle3.setValue("numereci", numereci);
					tblControle3.setValue("codiforn", codiforn);
					tblControle3.setValue("codiespe", codiespe);
					tblControle3.setValue("data", data);
					tblControle3.setValue("alterado", "S");
					tblControle3.setValue("vlrcusto", vlrCusto);
					tblControle3.setValue("vlrvenda", vlrVenda);
					tblControle3.execute();
				}
			}
		}
	}
	
	public static void updateNomeMedicos() {
		ApplicationUI ui = (ApplicationUI) UI.getCurrent();
		
		Table tblMedicos2 = ui.database.loadTableByName("medicos");
		tblMedicos2.setAuditing(false);
		
		Table tblMedicos = ui.database.loadTableByName("medicos");
		tblMedicos.select("codimedi, nome");
		tblMedicos.loadData();
		while (!tblMedicos.eof()) {
			try {
				String nome = tblMedicos.getString("nome");
				nome = nome.substring(0, nome.indexOf(" ")).trim();
				int codimedi = tblMedicos.getInteger("codimedi");
				int codimedi2 = Integer.valueOf(nome);
				
				if (codimedi==codimedi2) {
					System.out.println(tblMedicos.getString("nome"));
					nome = tblMedicos.getString("nome");
					nome = nome.substring(nome.indexOf(" ")+1);
							
					tblMedicos2.update();
					tblMedicos2.setString("nome", nome.trim());
					tblMedicos2.setFilter("uid", tblMedicos.getString("uid"));
					tblMedicos2.execute();
				}
			}
			catch (Exception e) {
				System.out.println(e.getMessage());
			}
			
			tblMedicos.next();
		}
	}
	
	public static void updateDescricaoEspecialidades() {
		ApplicationUI ui = (ApplicationUI) UI.getCurrent();
		
		Table tblEspecialidades2 = ui.database.loadTableByName("especialidades");
		tblEspecialidades2.setAuditing(false);
		
		Table tblEspecialidades = ui.database.loadTableByName("especialidades");
		tblEspecialidades.select("codiespe, descricao");
		tblEspecialidades.loadData();
		while (!tblEspecialidades.eof()) {
			try {
				String descricao = tblEspecialidades.getString("descricao");
				descricao = descricao.substring(0, descricao.indexOf(" ")).trim();
				int codiespe = tblEspecialidades.getInteger("codiespe");
				int codiespe2 = Integer.valueOf(descricao);
				
				if (codiespe==codiespe2) {
					System.out.println(tblEspecialidades.getString("descricao"));
					descricao = tblEspecialidades.getString("descricao");
					descricao = descricao.substring(descricao.indexOf(" ")+1).trim();
							
					tblEspecialidades2.update();
					tblEspecialidades2.setString("descricao", descricao.trim());
					tblEspecialidades2.setFilter("uid", tblEspecialidades.getString("uid"));
					tblEspecialidades2.execute();
				}
			}
			catch(Exception e) {
				System.out.println(e.getMessage());
			}
			
			tblEspecialidades.next();
		}
	}
	
	public void updateUidMarcCons() {
		ApplicationUI ui = (ApplicationUI) UI.getCurrent();

		Table tblMarccons = ui.database.loadTableByName("marccons");
		tblMarccons.alterTable();
		
		Table tblMarccons2 = ui.database.loadTableByName("marccons");
		
		while (true) {
			tblMarccons.select("sequencia");
			tblMarccons.setWhere("(marccons.uid is null)");
			tblMarccons.setLimit(100);
			tblMarccons.loadData();
			if (tblMarccons.eof()) {
				break;
			}
			
			while (!tblMarccons.eof()) {
				tblMarccons2.update();
				tblMarccons2.setValue("uid", UUID.randomUUID().toString().toUpperCase());
			    tblMarccons2.setFilter("sequencia", tblMarccons.getString("sequencia"));
				tblMarccons2.execute();
				
				tblMarccons.next();
			}
		}
		System.out.println("Processo concluido!");
	}

	public static void updateUidPermissoes() {
		ApplicationUI ui = (ApplicationUI) UI.getCurrent();
		Table tblPermissoes1 = ui.database.loadTableByName("permissoes");
		Table tblPermissoes2 = ui.database.loadTableByName("permissoes");
		try {
			ui.database.openConnection();
			
			tblPermissoes1.select("*");
			tblPermissoes1.loadData();
			while (!tblPermissoes1.eof()) {
				if (tblPermissoes1.getString("uid").length()<10) {
					tblPermissoes2.update();
					tblPermissoes2.setString("uid", UUID.randomUUID().toString().toUpperCase());
					tblPermissoes2.setFilter("idmenu", tblPermissoes1.getString("idmenu"));
					tblPermissoes2.setFilter("idgrupo", tblPermissoes1.getString("idgrupo"));
					tblPermissoes2.execute();
				}
				
				tblPermissoes1.next();
			}
		}
		finally {
			ui.database.closeConnection();
		}
	}
	
	public static void insertUsuarios(String login, String nome, String idGrupo, String senha) {
		ApplicationUI ui = (ApplicationUI) UI.getCurrent();
		Table tblUsuarios = ui.database.loadTableByName("usuarios");
		
		try {
			ui.database.openConnection();

			tblUsuarios.select("*");
			tblUsuarios.setFilter("projectName", "prosaude2"); 
			tblUsuarios.setFilter("login", login);
			tblUsuarios.loadData();
			if (tblUsuarios.eof()) {
				tblUsuarios.insert();
				tblUsuarios.setString("projectName", "prosaude2");
				tblUsuarios.setString("uid", UUID.randomUUID().toString().toUpperCase());
				tblUsuarios.setString("login", login);
				tblUsuarios.setString("nome", nome);
				tblUsuarios.setString("idgrupo", idGrupo);
				tblUsuarios.setString("senha", senha);
				tblUsuarios.execute();
			}
		}
		finally {
			ui.database.closeConnection();
		}
	}
	
	public static void insertPermissoes(String idGrupo, String idMenu) {
		ApplicationUI ui = (ApplicationUI) UI.getCurrent();
		Table tblPermissoes = ui.database.loadTableByName("permissoes");
		try {
			ui.database.openConnection();
			
			tblPermissoes.select("*"); 
			tblPermissoes.setFilter("projectName", "prosaude2");
			tblPermissoes.setFilter("idgrupo", idGrupo);
			tblPermissoes.setFilter("idmenu", idMenu);
			tblPermissoes.loadData();
			if (tblPermissoes.eof()) {
				tblPermissoes.insert();
				tblPermissoes.setString("uid", UUID.randomUUID().toString().toUpperCase());
				tblPermissoes.setString("projectName", "prosaude2");
				tblPermissoes.setString("idmenu", idMenu);
				tblPermissoes.setString("idgrupo", idGrupo);
				tblPermissoes.execute();
			}
		}
		finally {
			ui.database.closeConnection();
		}
	}
	
	public static void insertGrupo(String idGrupo, String descricao) {
		ApplicationUI ui = (ApplicationUI) UI.getCurrent();
		Table tblGrupUsua = ui.database.loadTableByName("grupusua");
		try {
			ui.database.openConnection();

			tblGrupUsua.select("*");
			tblGrupUsua.setFilter("projectName", "prosaude2");
			tblGrupUsua.setFilter("idgrupo", idGrupo);
			tblGrupUsua.loadData();
			if (tblGrupUsua.eof()) {
				tblGrupUsua.insert();
				tblGrupUsua.setString("uid", UUID.randomUUID().toString().toUpperCase());
				tblGrupUsua.setString("projectName", "prosaude2");
				tblGrupUsua.setString("idgrupo", idGrupo);
				tblGrupUsua.setString("descricao", descricao);
				tblGrupUsua.execute();
			}
		}
		finally {
			ui.database.closeConnection();
		}
	}

	public static void insertMenuItem(String projectName, String idMenu, String texto, String ordem, String className, String parent_idMenu) {
		ApplicationUI ui = (ApplicationUI) UI.getCurrent();
		Table tblMenu = ui.database.loadTableByName("menu");
		try {
			ui.database.openConnection();
			
			tblMenu.select("*");
			tblMenu.setFilter("projectName", "prosaude2");
			tblMenu.setFilter("idmenu", idMenu);
			tblMenu.loadData();
			if (tblMenu.eof()) {
				tblMenu.insert();
				tblMenu.setString("uid", UUID.randomUUID().toString().toUpperCase());
				tblMenu.setString("projectName", projectName);
				tblMenu.setString("idmenu", idMenu);
				tblMenu.setString("texto", texto);
				tblMenu.setString("ordem",  ordem);
				
				if (className!=null) {
					tblMenu.setString("classname",  className);
				}
				
				if (parent_idMenu!=null) {
					tblMenu.setString("parent_idmenu", parent_idMenu);
				}
				
				tblMenu.execute();
			}
		}
		finally {
			ui.database.closeConnection();
		}
	}
	
	public static void createMenu() {
		ApplicationUI ui = (ApplicationUI) UI.getCurrent();

		Table tblParametros = ui.database.loadTableByName("parametros");
		tblParametros.alterTable();
		
		Table tblPermissoes = ui.database.loadTableByName("permissoes");
		tblPermissoes.alterTable();
		
		Table tblUsuarios = ui.database.loadTableByName("usuarios");
		tblUsuarios.alterTable();
		
		insertMenuItem("prosaude2", "1", "Cadastro", "menu1", null, null);
		insertMenuItem("prosaude2", "101", "Especialidades", "menu101", "especialidades", "1");
		insertMenuItem("prosaude2", "102", "Médicos", "menu102", "medicos", "1");
		insertMenuItem("prosaude2", "103", "Fornecedores", "menu103",  "fornecedores", "1");
		insertMenuItem("prosaude2", "104", "CEP", "menu104", "cep", "1");
		insertMenuItem("prosaude2", "105", "Prefixos",  "menu15",  "prefixo", "1");
		insertMenuItem("prosaude2", "106", "Associados", "menu16", "associados", "1");
		insertMenuItem("prosaude2", "107", "Caixas", "menu17", "caixas", "1");
		insertMenuItem("prosaude2", "108", "Guia de Atendimento", "menu18", "marccons", "1");
		
		insertMenuItem("prosaude2", "2", "Caixa", "menu2", null, null);
		insertMenuItem("prosaude2", "201", "Carrinho de compras", "menu21", "carrcomp2", "2");
		insertMenuItem("prosaude2", "202", "Recibos", "menu22", "recibos", "2");
		insertMenuItem("prosaude2", "203", "Devolução/extorno de recibos",  "menu23", "devoreci", "2");
		insertMenuItem("prosaude2", "204", "Retiradas do caixa", "menu24", "saidcaix", "2");
		
		insertMenuItem("prosaude2", "3", "Relatórios", "menu3", null, null);
		insertMenuItem("prosaude2", "301", "Atendimento por período", "menu301", "", "3");
		insertMenuItem("prosaude2", "302", "Estatística de consultas por especialidades", "menu302", "", "3");
		insertMenuItem("prosaude2", "303", "Relação de associados", "menu303", "", "3");
		insertMenuItem("prosaude2", "304", "Relação de associados", "menu304", "", "3");
		insertMenuItem("prosaude2", "305", "Associados inscritos por período", "menu305", "", "3");
		insertMenuItem("prosaude2", "306", "Associados atualizados por período", "menu306", "", "3");
		insertMenuItem("prosaude2", "307", "Associados por prefixo", "menu307", "", "3");
		insertMenuItem("prosaude2", "308", "Estatística de densidade por bairro", "menu308", "", "3");
		insertMenuItem("prosaude2", "309", "Estatística de densidade por municipio", "menu309", "", "3");
		insertMenuItem("prosaude2", "310", "Etiquetas por casa", "menu310", "", "3");
		insertMenuItem("prosaude2", "311", "Etiquetas individuais", "menu311", "", "3");
		insertMenuItem("prosaude2", "312", "Fechamento do caixa", "menu312", "relafechcaix", "3");
		insertMenuItem("prosaude2", "313", "Resumo fechamento do caixa", "menu313", "relafechcaix2", "3");
		
		insertMenuItem("prosaude2", "4", "Segurança", "menu4", null, null);
		insertMenuItem("prosaude2", "401", "Opções", "menu401", "menu",  "4");
		insertMenuItem("prosaude2", "402", "Grupos de usuários", "menu402", "grupusua", "4");
		insertMenuItem("prosaude2", "403", "Usuários", "menu403", "usuarios", "4");
		
		insertGrupo("1", "ADMINISTRADORES");
		insertGrupo("2", "CADASTRO");
		insertGrupo("3", "CAIXA");
		
		insertPermissoes("1", "1");
		insertPermissoes("1", "101");
		insertPermissoes("1", "102");
		insertPermissoes("1", "103");
		insertPermissoes("1", "104");
		insertPermissoes("1", "105");
		insertPermissoes("1", "106");
		insertPermissoes("1", "107");
		
		insertPermissoes("1", "2");
		insertPermissoes("1", "201");
		insertPermissoes("1", "202");
		insertPermissoes("1", "203");
		insertPermissoes("1", "204");
		
		insertPermissoes("1", "3");
		insertPermissoes("1", "301");
		insertPermissoes("1", "302");
		insertPermissoes("1", "303");
		insertPermissoes("1", "304");
		insertPermissoes("1", "305");
		insertPermissoes("1", "306");
		insertPermissoes("1", "307");
		insertPermissoes("1", "308");
		insertPermissoes("1", "309");
		insertPermissoes("1", "310");
		insertPermissoes("1", "311");
		insertPermissoes("1", "312");
		insertPermissoes("1", "313");
		
		insertPermissoes("1", "4");
		insertPermissoes("1", "402");
		insertPermissoes("1", "403");
		
		insertUsuarios("ADMINISTRADOR", "Administrador", "1", "Rataplan23");
	}

	public static void updateUidDevoReci() {
		ApplicationUI ui = (ApplicationUI) UI.getCurrent();
		Table tblDevoReci = ui.database.loadTableByName("devoreci");
		Table tblDevoReci2 = ui.database.loadTableByName("devoreci");
		
		tblDevoReci.select("*");
		tblDevoReci.loadData();
		while (!tblDevoReci.eof()) {
			if (tblDevoReci.getString("uid").isEmpty()) {
				tblDevoReci2.update();
				tblDevoReci2.setValue("uid", UUID.randomUUID().toString().toUpperCase());
				tblDevoReci2.setFilter("numereci", tblDevoReci.getInteger("numereci"));
				tblDevoReci2.execute();
			}
			
			tblDevoReci.next();
		}
	}
	
	public static void updateUidSaidCaix() {
		ApplicationUI ui = (ApplicationUI) UI.getCurrent();
		Table tblDevoReci = ui.database.loadTableByName("saidcaix");
		Table tblDevoReci2 = ui.database.loadTableByName("saidcaix");
		
		tblDevoReci.select("*");
		tblDevoReci.loadData();
		while (!tblDevoReci.eof()) {
			if (tblDevoReci.getString("uid").isEmpty()) {
				tblDevoReci2.update();
				tblDevoReci2.setValue("uid", UUID.randomUUID().toString().toUpperCase());
				tblDevoReci2.setFilter("sequencia", tblDevoReci.getInteger("sequencia"));
				tblDevoReci2.execute();
			}
			
			tblDevoReci.next();
		}
	}
	
	public static void updateUidEspeForn() {
		ApplicationUI ui = (ApplicationUI) UI.getCurrent();
		Table tblEspeForn = ui.database.loadTableByName("espeforn");
		Table tblEspeForn2 = ui.database.loadTableByName("espeforn");
		
		tblEspeForn.select("*");
		tblEspeForn.loadData();
		while (!tblEspeForn.eof()) {
			if (tblEspeForn.getString("uid").isEmpty()) {
				tblEspeForn2.update();
				tblEspeForn2.setValue("uid", UUID.randomUUID().toString().toUpperCase());
				tblEspeForn2.setFilter("sequencia", tblEspeForn.getInteger("sequencia"));
				tblEspeForn2.execute();
			}
			
			tblEspeForn.next();
		}
	}
	
	public static void updateUidRecibos() {
		ApplicationUI ui = (ApplicationUI) UI.getCurrent();
		Table tblRecibos = ui.database.loadTableByName("recibos");
		Table tblRecibos2 = ui.database.loadTableByName("recibos");
		
		tblRecibos.select("*");
		tblRecibos.setWhere("(recibos.uid is null)");
		tblRecibos.loadData();
		while (!tblRecibos.eof()) {
			tblRecibos2.update();
			tblRecibos2.setValue("uid", UUID.randomUUID().toString().toUpperCase());
			tblRecibos2.setFilter("sequencia", tblRecibos.getInteger("sequencia"));
			tblRecibos2.execute();
			
			tblRecibos.next();
		}
	}
	
	public static void updateUidMarcCons2() {
		ApplicationUI ui = (ApplicationUI) UI.getCurrent();
		Table tblRecibos = ui.database.loadTableByName("marccons");
		Table tblRecibos2 = ui.database.loadTableByName("marccons");
		
		tblRecibos.select("*");
		tblRecibos.setWhere("(marccons.uid is null)");
		tblRecibos.loadData();
		while (!tblRecibos.eof()) {
			tblRecibos2.update();
			tblRecibos2.setValue("uid", UUID.randomUUID().toString().toUpperCase());
			tblRecibos2.setFilter("sequencia", tblRecibos.getInteger("sequencia"));
			tblRecibos2.execute();
			
			tblRecibos.next();
		}
	}
	
	public static void updateUidAssociados() {
		ApplicationUI ui = (ApplicationUI) UI.getCurrent();
		Table tblRecibos = ui.database.loadTableByName("associados");
		Table tblRecibos2 = ui.database.loadTableByName("associados");
		boolean continuar = true;
		while (continuar) {
			tblRecibos.select("*");
			tblRecibos.setWhere("(associados.uid is null)");
			tblRecibos.setLimit(100);
			tblRecibos.loadData();
			if (tblRecibos.eof()) {
				continuar = false;
			}
			while (!tblRecibos.eof()) {
				tblRecibos2.update();
				tblRecibos2.setValue("uid", UUID.randomUUID().toString().toUpperCase());
				tblRecibos2.setFilter("sequencia", tblRecibos.getInteger("sequencia"));
				tblRecibos2.execute();
				
				tblRecibos.next();
			}
		}
	}
	
	public static void updateUidMedicos() {
		ApplicationUI ui = (ApplicationUI) UI.getCurrent();
		Table tblRecibos = ui.database.loadTableByName("medicos");
		Table tblRecibos2 = ui.database.loadTableByName("medicos");
		
		tblRecibos.select("*");
		tblRecibos.setWhere("(medicos.uid is null)");
		tblRecibos.loadData();
		while (!tblRecibos.eof()) {
			tblRecibos2.update();
			tblRecibos2.setValue("uid", UUID.randomUUID().toString().toUpperCase());
			tblRecibos2.setFilter("codimedi", tblRecibos.getInteger("codimedi"));
			tblRecibos2.execute();
			
			tblRecibos.next();
		}
	}
	
	public static void updateUidEspecialidades() {
		System.out.println("Atualizando especialidades...");
		
		ApplicationUI ui = (ApplicationUI) UI.getCurrent();
		Table tblFornecedores = ui.database.loadTableByName("fornecedores");
		Table tblEspecialidades = ui.database.loadTableByName("especialidades");
		Table tblEspecialidades2 = ui.database.loadTableByName("especialidades");
		tblEspecialidades.alterTable();
		
		tblEspecialidades.select("*");
		tblEspecialidades.loadData();
		while (!tblEspecialidades.eof()) {
			tblFornecedores.select("uid");
			tblFornecedores.setFilter("codiforn", tblEspecialidades.getInteger("codiforn"));
			tblFornecedores.loadData();
			
			tblEspecialidades2.update();
			tblEspecialidades2.setValue("uid", UUID.randomUUID().toString().toUpperCase());
			tblEspecialidades2.setValue("uidfornecedor", tblFornecedores.getString("uid"));
			tblEspecialidades2.setFilter("codiespe", tblEspecialidades.getInteger("codiespe"));
			tblEspecialidades2.execute();
			
			tblEspecialidades.next();
		}
		System.out.println("processo concluido!");
	}
}
