package com.evolucao.weblibrary.tables;

import com.evolucao.rmlibrary.database.Database;
import com.evolucao.rmlibrary.database.ExistenceCheck;
import com.evolucao.rmlibrary.database.ForeingSearch;
import com.evolucao.rmlibrary.database.RmGridRegistry;
import com.evolucao.rmlibrary.database.Table;
import com.evolucao.rmlibrary.database.TableRegistry;
import com.evolucao.rmlibrary.database.enumerators.AllowLike;
import com.evolucao.rmlibrary.database.enumerators.FieldType;
import com.evolucao.rmlibrary.database.events.AfterExistenceCheckEvent;
import com.evolucao.rmlibrary.database.events.CommandEvent;
import com.evolucao.rmlibrary.database.events.LoadRmGridEvent;
import com.evolucao.rmlibrary.database.events.LoadTableEvent;
import com.evolucao.rmlibrary.database.events.ValidateFieldEvent;
import com.evolucao.rmlibrary.database.events.ValidateFieldEvent.ValidateFieldEventListener;
import com.evolucao.rmlibrary.database.events.table.InitialValueEvent;
import com.evolucao.rmlibrary.database.events.table.InitialValueEvent.InitialValueEventListener;
import com.evolucao.rmlibrary.database.events.AfterExistenceCheckEvent.AfterExistenceCheckEventListener;
import com.evolucao.rmlibrary.database.events.CommandEvent.CommandEventListener;
import com.evolucao.rmlibrary.database.events.LoadRmGridEvent.LoadRmGridEventListener;
import com.evolucao.rmlibrary.database.events.LoadTableEvent.LoadTableEventListener;
import com.evolucao.rmlibrary.database.registry.CommandRegistry;
import com.evolucao.rmlibrary.ui.controller.ControlForm;
import com.evolucao.rmlibrary.ui.form.enumerators.SectionState;
import com.evolucao.rmlibrary.ui.production.RmGrid;
import com.evolucao.weblibrary.ApplicationUI;
import com.vaadin.ui.UI;

public class Usuarios {
	public static void configure(Database database) {
		TableRegistry tableRegistry = database.addTableRegistry("usuarios");
		tableRegistry.addLoadTableEventListener(new LoadTableEventListener() {
			@Override
			public void onLoadTable(LoadTableEvent event) {
				Table tblTabela = event.getTable();
				tblTabela.setTableName("usuarios");
				tblTabela.addField("uid", FieldType.VARCHAR, 50);
				tblTabela.addField("login", FieldType.VARCHAR, 50);
				tblTabela.addField("nome", FieldType.VARCHAR, 50);
				tblTabela.addField("senha", FieldType.VARCHAR, 50);
				tblTabela.addField("uidTipoUsuario", FieldType.VARCHAR, 50);
				tblTabela.addField("ultiaces", FieldType.DATETIME, 10);
				tblTabela.addField("ipultiaces", FieldType.VARCHAR, 11);
				tblTabela.addField("passport", FieldType.VARCHAR, 50);
				tblTabela.addField("idgrupo", FieldType.VARCHAR, 10);
				tblTabela.addField("uidGrupo", FieldType.VARCHAR, 50);
				tblTabela.addField("descidgrupo", FieldType.VARCHAR, 50);
				tblTabela.addField("projectName", FieldType.VARCHAR, 50);
				tblTabela.setPrimaryKey("uid");
				
				tblTabela.setPassword("senha", true);
				
				tblTabela.setAllowLike("projectName", AllowLike.NONE);
				tblTabela.setAllowLike("login", AllowLike.NONE);
				tblTabela.setAllowLike("senha", AllowLike.NONE);
				tblTabela.setAllowLike("idgrupo", AllowLike.NONE);
				
				tblTabela.addJoin("grupusua", "projectName_idgrupo", "projectName, idgrupo");
				tblTabela.fieldByName("descidgrupo").setExpression("grupusua.descricao");
				tblTabela.fieldByName("descidgrupo").setReadOnly(true);
				
				tblTabela.addIndex("login", "login");
				
				tblTabela.setMainFilter("projectName", "prosaude2");
				
				tblTabela.addInitialValueEventListener(new InitialValueEventListener() {
					@Override
					public void onInitialValue(InitialValueEvent event) {
						event.getTable().setValue("projectName", "prosaude2");
					}
				});
				
				ForeingSearch foreingSearch = tblTabela.fieldByName("idgrupo").addForeingSearch();
				foreingSearch.setTargetRmGridName("grupusua");
				foreingSearch.setTargetIndexName("projectName_idgrupo");
				foreingSearch.setRelationship("projectName, idgrupo");
				foreingSearch.setReturnFieldName("idgrupo");
				foreingSearch.setTitle("Pesquisa por grupo de usuários:");
				foreingSearch.setOrder("descricao");
				foreingSearch.setAutoOpenFilterForm(false);
				foreingSearch.setWidth("800px");
				foreingSearch.setHeight("655px");
				
				/*
				ExistenceCheck existenceCheck = tblTabela.fieldByName("idgrupo").addExistenceCheck("grupusua", "projectName_idgrupo", "projectName, idgrupo", "Grupo de usuários inválida!");
				existenceCheck.addAfterExistenceCheckEventListener(new AfterExistenceCheckEventListener() {
					@Override
					public void onAfterExistenceCheck(AfterExistenceCheckEvent event) {
						event.getSourceTable().setValue("descidgrupo", event.getTargetTable().getString("descricao"));
						event.getSourceTable().setValue("uidGrupo", event.getTargetTable().getString("uid"));
					}
				});
				*/
			}
		});
		
	    RmGridRegistry rmGridRegistry = database.addRmGridRegistry("usuarios");
	    rmGridRegistry.addLoadRmGridEventListener(new LoadRmGridEventListener() {
			@Override
			public void onLoadRmGrid(LoadRmGridEvent event) {
				Table table = event.getTable();
				if (table==null) {
					table = database.loadTableByName("usuarios");
				}
				
				RmGrid rmGrid = event.getRmGrid();
				rmGrid.setTable(table);
				rmGrid.setLimit(10);
				rmGrid.addField("login", "Login", 200d);
				rmGrid.addField("nome", "Nome", 200d, 1);
				rmGrid.addField("descidgrupo", "Grupo do usuário", 200d);
				rmGrid.addField("ultiaces", "Último Acesso", 150d);
				
				ControlForm controlForm = rmGrid.getForm();
				controlForm.setTitle("Manutenção de usuários:");
				controlForm.setWidth(600d);
				controlForm.setHeight(520d);
				
				controlForm.addNewLine();
				controlForm.addSection("usuarios", "Controle de usuários.", SectionState.MAXIMIZED);
				{
					controlForm.addNewLine();
					controlForm.addField("login", "Login", 150d, 1);
					
					controlForm.addNewLine();
					controlForm.addField("nome", "Nome do usuário", 100d, 1);

					controlForm.addNewLine();
					controlForm.addField("idgrupo", "Código", 150d);
					controlForm.addField("descidgrupo", "Grupo do usuário", 150d, 1);
					
					controlForm.addNewLine();
					controlForm.addField("senha", "Senha", 100d, 1);
					
				}
				
				controlForm = rmGrid.getFormFilter();
				controlForm.setTitle("Pesquisa por caixas:");
				controlForm.setWidth(600d);
				controlForm.setHeight(520d);
				
				controlForm.addNewLine();
				controlForm.addSection("usuarios", "Controle de usuários.", SectionState.MAXIMIZED);
				{
					controlForm.addNewLine();
					controlForm.addField("login", "Login", 150d, 1);
					
					controlForm.addNewLine();
					controlForm.addField("nome", "Nome do usuário", 100d, 1);
					
					controlForm.addNewLine();
					controlForm.addField("idgrupo", "Código", 150d);
					controlForm.addField("descidgrupo", "Grupo do usuário", 150d, 1);

					controlForm.addNewLine();
					controlForm.addField("senha", "Senha", 100d, 1);
				}
			}
		});
	    
		CommandRegistry commandRegistry = database.addCommandRegistry("usuarios");
		commandRegistry.addCommandEventListener(new CommandEventListener() {
			@Override
			public void onCommandExecute(CommandEvent event) {
				RmGrid rmGrid = database.loadRmGridByName("usuarios");
				rmGrid.updateContent();

				ApplicationUI ui = (ApplicationUI) UI.getCurrent();
				ui.getSystemView().addTab(rmGrid, "Usuários", true);
			}
		});
	}
}
