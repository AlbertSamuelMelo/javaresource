package com.evolucao.weblibrary.tables;

import com.evolucao.rmlibrary.database.Database;
import com.evolucao.rmlibrary.database.RmGridRegistry;
import com.evolucao.rmlibrary.database.Table;
import com.evolucao.rmlibrary.database.TableRegistry;
import com.evolucao.rmlibrary.database.enumerators.AllowLike;
import com.evolucao.rmlibrary.database.enumerators.FieldType;
import com.evolucao.rmlibrary.database.events.CommandEvent;
import com.evolucao.rmlibrary.database.events.LoadRmGridEvent;
import com.evolucao.rmlibrary.database.events.LoadTableEvent;
import com.evolucao.rmlibrary.database.events.CommandEvent.CommandEventListener;
import com.evolucao.rmlibrary.database.events.LoadRmGridEvent.LoadRmGridEventListener;
import com.evolucao.rmlibrary.database.events.LoadTableEvent.LoadTableEventListener;
import com.evolucao.rmlibrary.database.registry.CommandRegistry;
import com.evolucao.rmlibrary.ui.controller.ControlForm;
import com.evolucao.rmlibrary.ui.form.enumerators.SectionState;
import com.evolucao.rmlibrary.ui.production.RmGrid;
import com.evolucao.weblibrary.ApplicationUI;
import com.vaadin.ui.UI;

public class GrupUsua {
	public static void configure(Database database) {
		TableRegistry tableRegistry = database.addTableRegistry("grupusua");
		tableRegistry.addLoadTableEventListener(new LoadTableEventListener() {
			@Override
			public void onLoadTable(LoadTableEvent event) {
				Table tblTabela = event.getTable();
				tblTabela.setTableName("grupusua");
				tblTabela.addField("projectName", FieldType.VARCHAR, 50);
				tblTabela.addField("idgrupo", FieldType.VARCHAR, 10);
				tblTabela.addField("descricao", FieldType.VARCHAR, 50);
				tblTabela.addField("padrao", FieldType.BOOLEAN, 10);
				tblTabela.setPrimaryKey("uid");
				
				tblTabela.addIndex("projectName_idgrupo", "projectName, idgrupo");
				tblTabela.addIndex("uid", "uid");
				
				Table tblPermissoes = database.loadTableByName("permissoes");
				tblTabela.addTableChild(tblPermissoes, "uidgrupo", false);
				
				tblTabela.setAllowLike("projectName", AllowLike.NONE);
				tblTabela.setAllowLike("idgrupo", AllowLike.NONE);
				
				tblTabela.setMainFilter("projectName", "prosaude2");
			}
		});
		
	    RmGridRegistry rmGridRegistry = database.addRmGridRegistry("grupusua");
	    rmGridRegistry.addLoadRmGridEventListener(new LoadRmGridEventListener() {
			@Override
			public void onLoadRmGrid(LoadRmGridEvent event) {
				Table table = event.getTable();
				if (table==null) {
					table = database.loadTableByName("grupusua");
				}
				
				RmGrid rmGrid = event.getRmGrid();
				rmGrid.setTable(table);
				rmGrid.setLimit(10);
				rmGrid.addField("idgrupo", "Código", 150d);
				rmGrid.addField("descricao", "Descricao", 100d, 1);
				rmGrid.addField("padrao", "Padrão", 100d);
				
				ControlForm controlForm = rmGrid.getForm();
				controlForm.setTitle("Manutenção de grupos de usuários:");
				controlForm.setWidth(800d);
				controlForm.setHeight(720d);
				
				controlForm.addNewLine();
				controlForm.addSection("grupusua", "Controle de grupos de usuários.", SectionState.MAXIMIZED);
				{
					controlForm.addNewLine();
					controlForm.addField("idgrupo", "Código", 150d);
					controlForm.addField("descricao", "Descrição", 100d, 1);
					
					controlForm.exitSection();
					controlForm.addNewLine();
					controlForm.addSection("permissoes", "Permissoes de acesso para o grupo:", SectionState.MAXIMIZED, 600d);
					{
						controlForm.addRmGrid("permissoes");
					}
				}
				
				controlForm = rmGrid.getFormFilter();
				controlForm.setTitle("Pesquisa por caixas:");
				controlForm.setWidth(800d);
				controlForm.setHeight(300d);
				
				controlForm.addNewLine();
				controlForm.addSection("grupusua", "Controle de grupos de usuários.", SectionState.MAXIMIZED);
				{
					controlForm.addNewLine();
					controlForm.addField("idgrupo", "Código", 150d);
					controlForm.addField("descricao", "Descrição", 100d, 1);
				}
			}
		});
	    
		CommandRegistry commandRegistry = database.addCommandRegistry("grupusua");
		commandRegistry.addCommandEventListener(new CommandEventListener() {
			@Override
			public void onCommandExecute(CommandEvent event) {
				RmGrid rmGrid = database.loadRmGridByName("grupusua");
				rmGrid.updateContent();

				ApplicationUI ui = (ApplicationUI) UI.getCurrent();
				ui.getSystemView().addTab(rmGrid, "Grupos de usuários", true);
			}
		});
	}
}
