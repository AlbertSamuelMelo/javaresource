package com.evolucao.weblibrary.view;

import com.evolucao.rmlibrary.database.Table;
import com.evolucao.rmlibrary.ui.MainBox;
import com.evolucao.rmlibrary.ui.production.RmGrid;
import com.evolucao.weblibrary.ApplicationUI;
import com.example.myapplication.Atualizacao;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.UI;

public class LoginView extends CssLayout implements View {
	@Override
	public void enter(ViewChangeEvent event) {
	}
	
	public LoginView() {
		addStyleName("mainview");
		
		CssLayout main = new CssLayout();
		main.addStyleName("flex-direction-col");
		main.addStyleName("form-login");
		main.addStyleName("centered");
		addComponent(main);
		{
			ApplicationUI ui = (ApplicationUI) UI.getCurrent();

			Table tblParametros = ui.database.loadTableByName("parametros");
			tblParametros.checkTable();
			
			tblParametros.select("nomeEmpresa, projectName");
			tblParametros.loadData();
			
			Table tblUsuarios = ui.database.loadTableByName("usuarios");
			tblUsuarios.select("nome");
			tblUsuarios.setFilter("login", "ADMINISTRADOR");
			tblUsuarios.setFilter("projectName", tblParametros.getString("projectName"));
			tblUsuarios.loadData();
			if (tblUsuarios.eof()) {
				Atualizacao.createMenu();
			}
			
			MainBox mainBox = new MainBox(tblParametros.getString("nomeEmpresa"));
			main.addComponent(mainBox);
			{
				RmGrid rmGrid = ui.database.loadRmGridByName("login");
				if (rmGrid.getTable().insert()) {
					rmGrid.getForm().setTable(rmGrid.getTable());
					mainBox.getBody().addComponent(rmGrid.getForm().deploy());
				}
				
				/*
				RmFormBox formBox = new RmFormBox("ACESSO AO SISTEMA:");
				Table tblLogin = ui.database.loadTableByName("login");
				formBox.setTable(tblLogin);
				tblLogin.update();
				mainBox.getBody().addComponent(formBox);
				{
					formBox.addField("login", "Login", 500);
					
					formBox.addNewLine();
					formBox.addField("senha", "Senha", 500);
				}
				formBox.getButtonSet().setShowRequiredFieldMessage(true);
				
				RmButtonSet buttonSet = new RmButtonSet();
				formBox.addComponent(buttonSet);
				{
					NormalButton btn = new NormalButton("Entrar no sistema");
					buttonSet.addRigthButton(btn);
					btn.setClickShortcut(KeyCode.ENTER);
					btn.addClickListener(new ClickListener() {
						@Override
						public void buttonClick(ClickEvent event) {
							if (formBox.getTable().validate()) {
								ApplicationUI ui = (ApplicationUI) UI.getCurrent();
								//Database database = (Database) VaadinSession.getCurrent().getAttribute("database");
								try {
									ui.database.openConnection();
									Table tblUsuarios = ui.database.loadTableByName("usuarios");
									tblUsuarios.select("senha, idgrupo");
									tblUsuarios.setFilter("login", formBox.getTable().getString("login"));
									tblUsuarios.setFilter("projectName", ui.getProjectName());
									tblUsuarios.loadData();
									if (!tblUsuarios.eof()) {
										if (!tblUsuarios.getString("senha").equals(formBox.getTable().getString("senha"))) {
											System.out.println("Usuário ou senha invalidos!");
										}
										else {
											System.out.println("Login realizado com sucesso!");
											String uidUsuario = tblUsuarios.getString("uid");
											String passport = UUID.randomUUID().toString().toUpperCase();

											SimpleRecord simpleRecord = new SimpleRecord();
											simpleRecord.setValue("uid", tblUsuarios.getString("uid"));
											simpleRecord.setValue("passport", passport);
											simpleRecord.setValue("idgrupo", tblUsuarios.getString("idgrupo"));
											
											tblUsuarios.update();
											tblUsuarios.setValue("passport", passport);
											tblUsuarios.setValue("ultiaces", new Date());
											tblUsuarios.setFilter("uid", uidUsuario);
											tblUsuarios.execute();
											
											//Utils.saveCookie("login", simpleRecord.encode());
											//VaadinSession.getCurrent().setAttribute("login", simpleRecord);
											//Zapdoctor2UI.Instance.login = simpleRecord;
											//Zapdoctor2UI.Instance.updateContent();
											
											ui.login = simpleRecord;
											
											//SystemView systemViewNew = new SystemView();
											//ui.getSystemView() = systemViewNew;
											
											ui.getDatabase().getUserSystemLogged().setUserName(formBox.getTable().getString("login"));
											
											WebBrowser webBrowser = Page.getCurrent().getWebBrowser();
											String ipAddress = webBrowser.getAddress();
											if (ipAddress != null) {
												ui.getDatabase().getUserSystemLogged().setIpAddress(ipAddress);
											}											
											
											ui.updateContent();
										}
									}
								}
								catch (Exception e) {
									System.out.println(e.getMessage());
								}
								finally {
									ui.database.closeConnection();
								}
							}
						}
					});
				}

				formBox.updateContent();
				*/
			}
		}
	}
}
